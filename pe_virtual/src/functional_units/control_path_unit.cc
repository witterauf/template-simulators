#include "control_path_unit.h"

namespace tcpa {
namespace simulator {
    
static constexpr unsigned int opcode_move = 0b000;
static constexpr unsigned int opcode_const = 0b001;
static constexpr unsigned int opcode_nop = 0b111;

void control_path_unit::clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_move: do_move(instruction); break;
        case opcode_const: do_const(instruction); break;
        case opcode_nop: default: break;
    }
}

void control_path_unit::do_move(functional_instruction instruction)
{
    do_two_registers(instruction, [](data_t val){ return val; });
}

void control_path_unit::do_const(functional_instruction instruction)
{
    do_constant(instruction);
}

}} // namespace tcpa::simulator;