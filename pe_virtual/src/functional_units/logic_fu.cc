#include "logic_fu.h"

namespace tcpa {
namespace simulator {

static constexpr unsigned int opcode_and = 0b000;
static constexpr unsigned int opcode_or = 0b001;
static constexpr unsigned int opcode_xor = 0b010;
static constexpr unsigned int opcode_neg = 0b011;
static constexpr unsigned int opcode_andi = 0b100;
static constexpr unsigned int opcode_ori = 0b101;
static constexpr unsigned int opcode_xori = 0b110;
static constexpr unsigned int opcode_nop = 0b111;

void logic_fu::clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_and: do_and(instruction); break;
        case opcode_or: do_or(instruction); break;
        case opcode_xor: do_xor(instruction); break;
        case opcode_neg: do_neg(instruction); break;
        case opcode_andi: do_andi(instruction); break;
        case opcode_ori: do_ori(instruction); break;
        case opcode_xori: do_xori(instruction); break;
        case opcode_nop: default: break;
    }
}

void logic_fu::do_and(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a & b; });
}

void logic_fu::do_or(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a | b; });
}

void logic_fu::do_xor(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a ^ b; });
}

void logic_fu::do_neg(functional_instruction instruction)
{
    do_two_registers(instruction, [](data_t val){ return ~val; });
}

void logic_fu::do_andi(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a & b; });
}

void logic_fu::do_ori(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a | b; });
}

void logic_fu::do_xori(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a ^ b; });
}

}} // namespace tcpa::simulator