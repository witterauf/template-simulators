#include "divider.h"

namespace tcpa {
namespace simulator {
    
static constexpr unsigned int opcode_div = 0b000;
static constexpr unsigned int opcode_nop = 0b111;

void divider::clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_div: do_div(instruction); break;
        case opcode_nop: default: break;
    }
}

void divider::do_div(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a / b; });
}

}} // namespace tcpa::simulator