#pragma once

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

class logic_fu: public functional_unit
{
public:
    using data_t = uint32_t;

    logic_fu(const architecture& arch, register_file* registers)
        : functional_unit{ arch, registers} {}
    
    void reset() override {}
    void clock(functional_instruction instruction) override;

private:
    void do_and(functional_instruction instruction);
    void do_or(functional_instruction instruction);
    void do_xor(functional_instruction instruction);
    void do_neg(functional_instruction instruction);
    void do_andi(functional_instruction instruction);
    void do_ori(functional_instruction instruction);
    void do_xori(functional_instruction instruction);
};

}} // namespace tcpa::simulator