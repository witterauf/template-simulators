#include "adder.h"

namespace tcpa {
namespace simulator {

static constexpr unsigned int opcode_add = 0b000;
static constexpr unsigned int opcode_sub = 0b001;
static constexpr unsigned int opcode_addi = 0b010;
static constexpr unsigned int opcode_subi = 0b011;
static constexpr unsigned int opcode_const = 0b100;
static constexpr unsigned int opcode_nop = 0b111;

void adder::clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_add: do_add(instruction); break;
        case opcode_sub: do_sub(instruction); break;
        case opcode_addi: do_addi(instruction); break;
        case opcode_subi: do_subi(instruction); break;
        case opcode_const: do_const(instruction); break;
        case opcode_nop: default: flags_ = { false }; break;
    }
}

void adder::do_add(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->add(a, b); });
}

void adder::do_addi(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->add(a, b); });
}

auto adder::add(data_t a, data_t b) -> data_t
{
    data_t result = a + b;
    flags_.zero = result == 0;
    flags_.negative = static_cast<typename std::make_signed<data_t>::type>(result) < 0;
    flags_.carry = static_cast<typename std::make_unsigned<data_t>::type>(result)
                        < static_cast<typename std::make_unsigned<data_t>::type>(a);
    flags_.overflow = ((~(a ^ b) & (result ^ a)) >> (sizeof(data_t) * 8 - 1)) != 0;
    return result;
}

void adder::do_sub(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->sub(a, b); });
}

void adder::do_subi(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->sub(a, b); });
}

auto adder::sub(data_t a, data_t b) -> data_t
{
    return add(a, ~b + 1);
}

void adder::do_const(functional_instruction instruction)
{
    do_constant(instruction);
    flags_ = { false };
}

}} // namespace tcpa::simulator