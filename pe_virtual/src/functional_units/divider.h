#pragma once

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

class divider: public functional_unit
{
public:
    using data_t = uint32_t;

    divider(const architecture& arch, register_file* registers)
        : functional_unit{ arch, registers} {}
    
    void reset() override {}
    void clock(functional_instruction instruction) override;

private:
    void do_div(functional_instruction instruction);
};

}} // namespace tcpa::simulator