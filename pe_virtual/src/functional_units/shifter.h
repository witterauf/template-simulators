#pragma once

#include "functional_unit.h"
#include <type_traits>

namespace tcpa {
namespace simulator {

class shifter: public functional_unit
{
public:
    using data_t = uint32_t;
    using data_t_unsigned = std::make_unsigned<data_t>::type;
    using data_t_signed = std::make_signed<data_t>::type;

    shifter(const architecture& arch, register_file* registers)
        : functional_unit{ arch, registers} {}
    
    void reset() override {}
    void clock(functional_instruction instruction) override;

private:
    void do_shl(functional_instruction instruction);
    void do_shr(functional_instruction instruction);
    void do_ashr(functional_instruction instruction);
    void do_shli(functional_instruction instruction);
    void do_shri(functional_instruction instruction);
    void do_ashri(functional_instruction instruction);

    data_t do_shr(data_t a, data_t b) const;
    data_t do_ashr(data_t a, data_t b) const;
};

}} // namespace tcpa::simulator