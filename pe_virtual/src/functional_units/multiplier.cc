#include "multiplier.h"

namespace tcpa {
namespace simulator {
    
static constexpr unsigned int opcode_mult = 0b000;
static constexpr unsigned int opcode_multi = 0b001;
static constexpr unsigned int opcode_mucst = 0b010;
static constexpr unsigned int opcode_nop = 0b111;

void multiplier::clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_mult: do_mult(instruction); break;
        case opcode_multi: do_multi(instruction); break;
        case opcode_mucst: do_mucst(instruction); break;
        case opcode_nop: default: break;
    }
}

void multiplier::do_mult(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a * b; });
}

void multiplier::do_multi(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a * b; });
}

void multiplier::do_mucst(functional_instruction instruction)
{
    do_constant(instruction);
}

}} // namespace tcpa::simulator