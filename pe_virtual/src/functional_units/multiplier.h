#pragma once

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

class multiplier: public functional_unit
{
public:
    using data_t = uint32_t;

    multiplier(const architecture& arch, register_file* registers)
        : functional_unit{ arch, registers} {}
    
    void reset() {}
    void clock(functional_instruction instruction);

private:
    void do_mult(functional_instruction instruction);
    void do_multi(functional_instruction instruction);
    void do_mucst(functional_instruction instruction);
};

}} // namespace tcpa::simulator