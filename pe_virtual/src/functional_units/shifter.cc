#include "shifter.h"

namespace tcpa {
namespace simulator {
    
static constexpr unsigned int opcode_shl = 0b000;
static constexpr unsigned int opcode_shr = 0b001;
static constexpr unsigned int opcode_ashr = 0b010;
static constexpr unsigned int opcode_shli = 0b011;
static constexpr unsigned int opcode_shri = 0b100;
static constexpr unsigned int opcode_ashri = 0b101;
static constexpr unsigned int opcode_nop = 0b111;

void shifter::clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_shl: do_shl(instruction); break;
        case opcode_shr: do_shr(instruction); break;
        case opcode_ashr: do_ashr(instruction); break;
        case opcode_shli: do_shli(instruction); break;
        case opcode_shri: do_shri(instruction); break;
        case opcode_ashri: do_ashri(instruction); break;
        case opcode_nop: default: break;
    }
}

void shifter::do_shl(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a << b; });
}

void shifter::do_shr(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->do_shr(a, b); });
}

void shifter::do_ashr(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->do_ashr(a, b); });
}

void shifter::do_shli(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a << b; });
}

void shifter::do_shri(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->do_shr(a, b); });
}

void shifter::do_ashri(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->do_ashr(a, b); });
}

auto shifter::do_shr(data_t a, data_t b) const -> data_t
{
    return static_cast<data_t_unsigned>(a) >> static_cast<data_t_unsigned>(b);
}

auto shifter::do_ashr(data_t a, data_t b) const -> data_t
{
    return static_cast<data_t_signed>(a) >> b;
}

}} // namespace tcpa::simulator