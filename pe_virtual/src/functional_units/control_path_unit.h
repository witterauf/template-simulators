#pragma once

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

class control_path_unit: public functional_unit
{
public:
    using data_t = uint32_t;

    control_path_unit(const architecture& arch, register_file* registers)
        : functional_unit{ arch, registers} {}
    
    void reset() override {}
    void clock(functional_instruction instruction) override;

private:
    void do_move(functional_instruction instruction);
    void do_const(functional_instruction instruction);
};

}} // namespace tcpa::simulator