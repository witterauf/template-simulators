#pragma once

#include "registers/register_file.h"
#include "vliw.h"
#include "bit_tools.h"

namespace tcpa {
namespace simulator {

class functional_unit
{
public:
    struct architecture
    {
        unsigned int instruction_width;
        unsigned int opcode_width;
        unsigned int register_field_width;
        unsigned int constant_width;
        unsigned int immediate_width;
    };
    
    functional_unit(const architecture& arch, register_file* registers)
        : registers_ { registers }
    {
        set_architecture(arch);
    }

    void set_architecture(const architecture& arch)
    {
        arch_ = arch;
        three_regs.destination.offset = arch_.instruction_width - arch_.opcode_width;
        three_regs.destination.width = arch_.register_field_width;
        three_regs.source_a.offset = three_regs.destination.offset - arch_.register_field_width;
        three_regs.source_a.width = arch_.register_field_width;
        three_regs.source_b.offset = three_regs.source_a.offset - arch_.register_field_width;
        three_regs.source_b.width = arch_.register_field_width;

        two_regs.destination.offset = arch_.instruction_width - arch_.opcode_width;
        two_regs.destination.width = arch_.register_field_width;
        two_regs.source.offset = two_regs.destination.offset - arch_.register_field_width;
        two_regs.source.width = arch_.register_field_width;

        immediate.destination.offset = arch_.instruction_width - arch_.opcode_width;
        immediate.destination.width = arch_.register_field_width;
        immediate.source.offset = immediate.destination.offset - arch_.register_field_width;
        immediate.source.width = arch_.register_field_width;
        immediate.value.offset = immediate.source.offset - arch_.register_field_width;
        immediate.value.width = arch_.instruction_width - arch_.opcode_width - arch_.opcode_width - 2 * arch_.register_field_width;

        constant.destination.offset = arch_.instruction_width - arch_.opcode_width;
        constant.destination.width = arch_.register_field_width;
        constant.value.offset = constant.destination.offset - arch_.register_field_width;
        constant.value.width = arch_.instruction_width - arch_.opcode_width - arch_.register_field_width;

        opcode.offset = arch_.instruction_width - arch_.opcode_width;
        opcode.width = arch_.opcode_width;
    }
    
    virtual void reset() = 0;
    virtual void clock(functional_instruction) = 0;
    virtual auto flagCount() const -> unsigned int { return 0; }
    virtual bool flag(unsigned int index) const { return false; }
    
protected:
    auto get_opcode(functional_instruction instruction) const -> unsigned int
    {
        return instruction.read(opcode);
    }

    // predefined operation types
    void do_constant(functional_instruction instruction);
    template<typename Operation>
    void do_three_registers(functional_instruction instruction, Operation op);
    template<typename Operation>
    void do_two_registers(functional_instruction instruction, Operation op);
	template<typename Operation>
    void do_immediate(functional_instruction instruction, Operation op);
    
private:
    struct
    {
        bit_field destination, source_a, source_b;
    } three_regs;
    
    struct
    {
        bit_field destination, source;
    } two_regs;
    
    struct
    {
        bit_field destination, value;
    } constant;
    
    struct
    {
        bit_field destination, source, value;
    } immediate;

    bit_field opcode;

    architecture arch_;
    register_file* registers_;
};

//##[ implementation ]#########################################################

template<typename Operation>
void functional_unit::do_three_registers(functional_instruction instruction, Operation op)
{
    auto destination = instruction.read(three_regs.destination);
    auto source_a = instruction.read(three_regs.source_a);
    auto source_b = instruction.read(three_regs.source_b);
    auto value_a = registers_->read_by_cumulative_index(source_a);
    auto value_b = registers_->read_by_cumulative_index(source_b);
    auto result = op(value_a, value_b);
    registers_->write_by_cumulative_index(destination, result);
}

template<typename Operation>
void functional_unit::do_two_registers(functional_instruction instruction, Operation op)
{
    auto destination = instruction.read(two_regs.destination);
    auto source = instruction.read(two_regs.source);
    auto value = registers_->read_by_cumulative_index(source);
    auto result = op(value);
    registers_->write_by_cumulative_index(destination, result);
}

template<typename Operation>
void functional_unit::do_immediate(functional_instruction instruction, Operation op)
{
    auto destination = instruction.read(immediate.destination);
    auto source_a = instruction.read(immediate.source);
    auto value_b = instruction.read(immediate.value);
    auto value_a = registers_->read_by_cumulative_index(source_a);
    auto result = op(value_a, value_b);
    registers_->write_by_cumulative_index(destination, result);
}

inline void functional_unit::do_constant(functional_instruction instruction)
{
    auto destination = instruction.read(constant.destination);
    auto value = instruction.read(constant.value);
    registers_->write_by_cumulative_index(destination, static_cast<uint32_t>(sign_extend(value, constant.value.width)));
}

}} // namespace tcpa::simulator