#pragma once

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

class adder : public functional_unit
{
public:
    using data_t = uint32_t;

    struct flags_t
    {
        bool zero;
        bool overflow;
        bool negative;
        bool carry;
    };

    adder(const architecture& arch, register_file* registers)
        : functional_unit{ arch, registers} {}
    
    void reset() override {}
    void clock(functional_instruction instruction) override;

    const flags_t &flags() const { return flags_; }    
    auto flagCount() const -> unsigned int override { return 4; }
    bool flag(unsigned int index) const
    {
        switch (index)
        {
            case 0: return flags_.zero;
            case 1: return flags_.overflow;
            case 2: return flags_.negative;
            case 3: return flags_.carry;
        };
        return false;
    }

private:
    data_t add(data_t a, data_t b); 
    data_t sub(data_t a, data_t b); 

    void do_add(functional_instruction instruction);
    void do_addi(functional_instruction instruction);
    void do_sub(functional_instruction instruction);
    void do_subi(functional_instruction instruction);
    void do_const(functional_instruction instruction);

    flags_t flags_;
};


}} // namespace tcpa::simulator