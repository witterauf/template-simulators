#include "branch_unit.h"

namespace tcpa {
namespace simulator {

static constexpr unsigned int opcode_width = 3;
static constexpr unsigned int opcode_branch = 0b000;
static constexpr unsigned int opcode_next = 0b111;

void branch_unit::set_architecture(const architecture& arch)
{
    flags_.resize(arch.fu_types_with_flags * arch.max_fus_per_type * arch.max_flags_per_fu);
    condition_width_ =
        number_of_bits(arch.fu_types_with_flags + 1)
        + number_of_bits(arch.max_fus_per_type)
        + number_of_bits(arch.max_flags_per_fu)
        + number_of_bits(arch.max_control_signals);
    target_count_ = 1 << arch.condition_count;
    instruction_width_ = 3 + arch.condition_count * condition_width_ + target_count_ * arch.target_width;    
    
    // calculate bit fields
    opcode_.offset = instruction_width_ - 3;
    opcode_.width = 3;
    
    condition_.control_signal_index.offset = 0;
    condition_.control_signal_index.width = number_of_bits(arch.max_control_signals);
    condition_.flag_index.offset = condition_.control_signal_index.width;
    condition_.flag_index.width = number_of_bits(arch.max_flags_per_fu);
    condition_.fu_index.offset = condition_.flag_index.offset + condition_.flag_index.width;
    condition_.fu_index.width = number_of_bits(arch.max_fus_per_type);
    condition_.fu_type.offset = condition_.fu_index.offset + condition_.fu_index.width;
    condition_.fu_type.width = number_of_bits(arch.fu_types_with_flags + 1);
    
    target_.offset = 0;
    target_.width = arch.target_width;

    arch_ = arch;
}

void branch_unit::reset()
{
    pc_ = 0;
}

void branch_unit::clock(branch_instruction instruction)
{
    auto op = instruction.read(opcode_);
    switch (op)
    {
        case opcode_branch: do_branch(instruction); break;
        case opcode_next:
        default: do_next(instruction); break;
    }
}

void branch_unit::do_branch(branch_instruction instruction)
{
    auto target_index = 0U;
    for (auto i = 0U; i < arch_.condition_count ; ++i)
    {
        target_index += check_condition(instruction, i) ? (1 << i) : 0;
    }
    pc_ = instruction.read_relative(target_, target_index * arch_.target_width);
}

void branch_unit::do_next(branch_instruction)
{
    pc_ += 1;
}

bool branch_unit::check_condition(branch_instruction instruction, unsigned int index)
{
    static constexpr unsigned int control_signal = 0b111;

    auto fu_type = read_from_condition(condition_.fu_type, instruction, index);
    if (fu_type == control_signal)
    {
        return check_control_signal(instruction, index);
    }
    else
    {
        return check_fu_flag(instruction, index, fu_type);
    }
}

bool branch_unit::check_fu_flag(branch_instruction instruction, unsigned int index, unsigned int fu_type) const
{
    auto fu_index = read_from_condition(condition_.fu_index, instruction, index);
    auto flag_index = read_from_condition(condition_.flag_index, instruction, index);
    auto overall_index = fu_type * arch_.max_fus_per_type * arch_.max_flags_per_fu
        + fu_index * arch_.max_flags_per_fu + flag_index;
    return flags_[overall_index];
}

bool branch_unit::check_control_signal(branch_instruction instruction, unsigned int index)
{
    auto control_signal_index = read_from_condition(condition_.control_signal_index, instruction, index);
    auto control_signal_value = control_reg_file_.read_by_cumulative_index(control_signal_index);
    return (control_signal_value & 1) != 0;
}

auto branch_unit::read_from_condition(const bit_field& field, branch_instruction instruction, unsigned int index) const
    -> uint32_t
{
    return instruction.read_relative(field, opcode_.offset - (index + 1) * condition_width_);
}

}} // namespace tcpa::simulator