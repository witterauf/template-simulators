#pragma once

#include "register.h"
#include "fifo.h"
#include "feedback_shift_register.h"
#include <stdexcept>
#include <array>

namespace tcpa {
namespace simulator {

class register_file
{
public:
    using data_t = uint32_t;

    struct architecture
    {
        std::vector<data_register::architecture> data_registers;
        std::vector<data_register::architecture> output_registers;
        std::vector<fifo::architecture> input_registers;
        std::vector<feedback_shift_register::architecture> feedback_shift_registers;
    };
    struct configuration
    {
        std::vector<fifo::configuration> input_registers;
        std::vector<feedback_shift_register::configuration> feedback_shift_registers;
    };
    struct state
    {
        std::vector<data_register::state> data_registers;
        std::vector<data_register::state> output_registers;
        std::vector<fifo::state> input_registers;
        std::vector<feedback_shift_register::state> feedback_shift_registers;
    };

    register_file() = default;
    register_file(const architecture& arch);
    void set_architecture(const architecture& arch);
    
    void reset();
    void clock();
    void configure(const configuration& config);
    void capture(state& s) const;

    void write_by_cumulative_index(unsigned int i, data_t value);
    auto read_by_cumulative_index(unsigned int i) -> data_t;

    auto input_register(unsigned int index) -> fifo& { return input_registers_[index]; }
    
private:
    struct feedback_shift_port
    {
        bool was_read;
        bool was_written;
        feedback_shift_register reg;
    };

    data_t feedback_shift_read(unsigned int i);
    void feedback_shift_write(unsigned int i, data_t data);

    bool is_feedback_shift_reg(unsigned int i) const;
    bool is_input_reg(unsigned int i) const;
    bool is_output_reg(unsigned int i) const;
    bool is_data_reg(unsigned int i) const;

    std::vector<data_register> data_registers_;
    std::vector<data_register> output_registers_;
    std::vector<fifo> input_registers_;
    std::vector<feedback_shift_port> feedback_shift_registers_;
    
    unsigned int id_of_first_data_reg_ = 0;
    unsigned int id_of_first_input_reg_ = 0;
    unsigned int id_of_first_output_reg_ = 0;
    unsigned int id_of_first_feedback_shift_reg_ = 0;
};

}} // namespace tcpa::simulator