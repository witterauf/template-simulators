#pragma once

#include <algorithm>
#include <array>
#include <stdexcept>
#include <vector>

namespace tcpa {
namespace simulator {

class shift_register
{
public:
    using data_t = uint32_t;
    using state = std::vector<data_t>;

    struct architecture
    {
        unsigned int max_length;
    };
    struct configuration
    {
        unsigned int length;
    };
    
    shift_register(const architecture& arch);
    void set_architecture(const architecture& arch);
    
    void reset(data_t initial_value = {});
    void clock();
    void capture(state& s) const;
    void configure(const configuration& config);

    void input(data_t signal);
    auto output() const -> data_t;

private:
    architecture arch_;
    configuration config_;
    std::vector<data_t> values_;
    size_t position_;
};

}} // namespace tcpa::simulator