#include "feedback_shift_register.h"
#include <algorithm>

namespace tcpa {
namespace simulator {

feedback_shift_register::feedback_shift_register(const architecture& arch)
{
    set_architecture(arch);
}

void feedback_shift_register::set_architecture(const architecture& arch)
{
    arch_ = arch;
}

void feedback_shift_register::reset(data_t)
{
    position_ = 0;
    input_ = 0;
}

void feedback_shift_register::clock(bool write, bool read)
{
    if (write)
    {
        fifo_[position_] = input_;
    }
    if (read || write)
    {
        position_ = (position_ + 1) % config_.length;
    }
}

void feedback_shift_register::configure(const configuration& config)
{
    fifo_ = std::vector<data_t>(config.length);
    if (config.length > arch_.max_length)
    {
        throw std::runtime_error{ "[feedback_shift_register::length(size_t)] length must not exceed maximum" };
    }
    config_ = config;
}

void feedback_shift_register::capture(state& s) const
{
    s.clear();
    std::copy(fifo_.begin(), fifo_.begin() + config_.length, std::back_inserter(s));
}

}} // namespace tcpa::simulator