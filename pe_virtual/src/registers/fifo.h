#ifndef __TCPASIM_PROCESSOR_REGISTERS_FIFO__
#define __TCPASIM_PROCESSOR_REGISTERS_FIFO__

#include "shift_register.h"

namespace tcpa {
namespace simulator {

// wrapper around shift_register
class fifo
{
public:
    using data_t = shift_register::data_t;
    using state = shift_register::state;
    using configuration = shift_register::configuration;
    using architecture = shift_register::architecture;

public:
    fifo(const architecture& arch)
        : sr_{ arch } {}
        
    void set_architecture(const architecture& arch) { sr_.set_architecture(arch); }

    void reset() { sr_.reset(); }
    void clock() { sr_.clock(); }
    void capture(state& s) const { sr_.capture(s); }

    void configure(configuration config)
	{
		// 1 cycle additional delay
        config.length += 1;
		sr_.configure(config);
	}

    void input(data_t value) { sr_.input(value); }
    auto output() const -> data_t { return sr_.output(); }

private:
    shift_register sr_;
};

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_REGISTERS_FIFO__
