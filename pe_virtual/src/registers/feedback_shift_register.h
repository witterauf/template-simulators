#pragma once

#include <algorithm>
#include <array>
#include <iterator>
#include <stdexcept>
#include <vector>

namespace tcpa {
namespace simulator {

class feedback_shift_register
{
public:
    using data_t = uint32_t;
    using ref = data_t&;
    using const_ref = data_t const&;
    using state = std::vector<data_t>;
    
    struct architecture
    {
        unsigned int max_length;
    };
    struct configuration
    {
        unsigned int length;
    };

	feedback_shift_register(const architecture& arch);    
    void set_architecture(const architecture& arch);

	void clock(bool write, bool read);
    void reset(data_t initial_value = {});
    void capture(state& s) const;
    void configure(const configuration& config);

    void input(data_t input) { input_ = input; }
    auto output() const -> data_t { return fifo_[position_]; }

private:
    architecture arch_;
    configuration config_;
	data_t input_ = 0;
    std::vector<data_t> fifo_;
	unsigned int position_ = 0;
};

}} // namespace tcpa::simulator