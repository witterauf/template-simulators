#include "register_file.h"

namespace tcpa {
namespace simulator {

register_file::register_file(const architecture& arch)
{
    set_architecture(arch);
}

void register_file::set_architecture(const architecture& arch)
{
    data_registers_.clear();
    input_registers_.clear();
    output_registers_.clear();
    feedback_shift_registers_.clear();
    
    for (auto i = 0U; i < arch.data_registers.size(); ++i)
    {
        data_registers_.push_back(data_register{ arch.data_registers[i] });
    } 
    for (auto i = 0U; i < arch.output_registers.size(); ++i)
    {
        output_registers_.push_back(data_register{ arch.output_registers[i]} );
    } 
    for (auto i = 0U; i < arch.input_registers.size(); ++i)
    {
        input_registers_.push_back(fifo{ arch.input_registers[i]} );
    } 
    for (auto i = 0U; i < arch.feedback_shift_registers.size(); ++i)
    {
        feedback_shift_registers_.push_back({ false, false, feedback_shift_register{ arch.feedback_shift_registers[i] } });
    }

    id_of_first_data_reg_ = 0;
    id_of_first_output_reg_ = data_registers_.size();
    id_of_first_input_reg_ = id_of_first_output_reg_ + output_registers_.size();
    id_of_first_feedback_shift_reg_ = id_of_first_input_reg_ + input_registers_.size();
}

void register_file::reset()
{
    for (auto &reg : input_registers_)
    {
        reg.reset();
    }
    for (auto &reg : data_registers_)
    {
        reg.reset();
    }
    for (auto &reg : output_registers_)
    {
        reg.reset();
    }
    for (auto &port : feedback_shift_registers_)
    {
        port.reg.reset();
        port.was_read = port.was_written = false;
    }
}

void register_file::clock()
{
    for (auto &reg : input_registers_)
    {
        reg.clock();
    }
    for (auto &reg : data_registers_)
    {
        reg.clock();
    }
    for (auto &reg : output_registers_)
    {
        reg.clock();
    }
    for (auto &port : feedback_shift_registers_)
    {
        port.reg.clock(port.was_written, port.was_read);
        port.was_read = port.was_written = false;
    }
}

void register_file::configure(const configuration& config)
{
    for (auto i = 0U; i < input_registers_.size(); ++i)
    {
        input_registers_[i].configure(config.input_registers[i]);
    } 
    for (auto i = 0U; i < feedback_shift_registers_.size(); ++i)
    {
        feedback_shift_registers_[i].reg.configure(config.feedback_shift_registers[i]);
    } 
}

auto register_file::read_by_cumulative_index(unsigned int i) -> data_t
{
    if (is_feedback_shift_reg(i))
    {
        return feedback_shift_read(i - id_of_first_feedback_shift_reg_);
    }
	else if (is_input_reg(i))
    {
		return input_registers_[i - id_of_first_input_reg_].output();
    }
	else if (is_output_reg(i))
    {
		return output_registers_[i - id_of_first_output_reg_].output();
    }
	else if (is_data_reg(i))
    {
        return data_registers_[i - id_of_first_data_reg_].output();
    }
    throw std::runtime_error{ "[register_file::read_by_cumulative_index(...)] invalid register index" };
}

auto register_file::feedback_shift_read(unsigned int i) -> data_t
{
    feedback_shift_registers_[i].was_read = true;
    return feedback_shift_registers_[i].reg.output();
}

void register_file::write_by_cumulative_index(unsigned int i, data_t value)
{
    if (is_feedback_shift_reg(i))
    {
        feedback_shift_write(i - id_of_first_feedback_shift_reg_, value);
    }
	else if (is_input_reg(i))
    {
		input_registers_[i - id_of_first_input_reg_].input(value);
    }
	else if (is_output_reg(i))
    {
		output_registers_[i - id_of_first_output_reg_].input(value);
    }
	else if (is_data_reg(i))
    {
        data_registers_[i - id_of_first_data_reg_].input(value);
    }
    else
    {
        throw std::runtime_error{ "[register_file::write_by_cumulative_index(...)] invalid register index" };
    }
}

void register_file::feedback_shift_write(unsigned int i, data_t data)
{
    feedback_shift_registers_[i].was_written = true;
    feedback_shift_registers_[i].reg.input(data);
}

bool register_file::is_feedback_shift_reg(unsigned int i) const
{
    return feedback_shift_registers_.size() >= 0
        && i >= id_of_first_feedback_shift_reg_;
}

bool register_file::is_input_reg(unsigned int i) const
{
    return input_registers_.size() >= 0
        && i >= id_of_first_input_reg_
        && i < id_of_first_feedback_shift_reg_;
}

bool register_file::is_output_reg(unsigned int i) const
{
    return output_registers_.size() >= 0
        && i >= id_of_first_output_reg_
        && i < id_of_first_input_reg_;
}

bool register_file::is_data_reg(unsigned int i) const
{
    return data_registers_.size() >= 0
        && i >= id_of_first_data_reg_
        && i < id_of_first_output_reg_;
}

void register_file::capture(state& s) const
{
	for (auto i = 0U; i < data_registers_.size(); ++i)
	{
		data_registers_[i].capture(s.data_registers[i]);
	}
	for (auto i = 0U; i < input_registers_.size(); ++i)
	{
		input_registers_[i].capture(s.input_registers[i]);
	}
	for (auto i = 0U; i < output_registers_.size(); ++i)
	{
		output_registers_[i].capture(s.output_registers[i]);
	}
	for (auto i = 0U; i < feedback_shift_registers_.size(); ++i)
	{
		feedback_shift_registers_[i].reg.capture(s.feedback_shift_registers[i]);
	}
}

}} // namespace tcpa::simulator