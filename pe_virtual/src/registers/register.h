#pragma once

//#include "proxy_port.h"
#include <cstdint>

namespace tcpa {
namespace simulator {

class data_register
{
public:
    using data_t = uint32_t;
    using state = data_t;
    //using proxy_port = proxy_port<data_t>;

    struct architecture
    {
        unsigned int data_width;
    };
    
    data_register(const architecture& arch) { set_architecture(arch); }
    void set_architecture(const architecture& arch) { arch_ = arch; mask_ = (1 << arch.data_width) - 1; }
    
    void reset() { input_ = 0; }
	void clock() { value_ = input_ & mask_; }
    void capture(state& s) const { s = value_; }

	void input(data_t val) { input_ = val; }
	auto output() const -> data_t { return value_; }

    /*proxy_port generate_proxy() const { return proxy_port{ &value_ }; }*/

private:
	data_t input_ = 0;
	data_t value_ = 0;
    
    architecture arch_;
    data_t mask_;
};

}} // namespace tcpa::simulator