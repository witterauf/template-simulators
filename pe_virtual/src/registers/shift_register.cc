#include "shift_register.h"

namespace tcpa {
namespace simulator {

shift_register::shift_register(const architecture& arch)
{
    set_architecture(arch);
}

void shift_register::set_architecture(const architecture& arch)
{
    arch_ = arch;
}

void shift_register::reset(data_t initial_value)
{
    position_ = 0;
}

void shift_register::configure(const configuration& config)
{
    values_ = std::vector<data_t>(config.length + 1, 0);
    if (config.length > arch_.max_length)
    {
        throw std::runtime_error{ "[shift_register::length(length)] length exceeds max_length" };
    }
    config_ = config;
}

void shift_register::clock()
{
    if (config_.length > 0)
    {
        position_ = (position_ + 1) % (config_.length + 1);
    }
}

void shift_register::input(data_t signal)
{
    values_[(position_ + config_.length) % (config_.length + 1)] = signal;
}

auto shift_register::output() const -> data_t
{
    return values_[position_];
}

void shift_register::capture(state& s) const
{
    s.clear();
	for (auto i = 0U; i <= config_.length; ++i)
	{
		s.push_back(values_[(position_ + i) % (config_.length + 1)]);
	}
}

}} // namespace tcpa::simulator