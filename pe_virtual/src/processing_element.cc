#include "processing_element.h"

namespace tcpa {
namespace simulator {

processing_element::processing_element(const architecture& arch)
{
    set_architecture(arch);
}

void processing_element::set_architecture(const architecture& arch)
{    
    for (auto i = 0U; i < arch.adder_count; ++i)
    {
        functional_units_.push_back(std::make_unique<adder>(arch.functional_units, &data_reg_file_));
    }
    for (auto i = 0U; i < arch.multiplier_count; ++i)
    {
        functional_units_.push_back(std::make_unique<multiplier>(arch.functional_units, &data_reg_file_));
    }
    for (auto i = 0U; i < arch.divider_count; ++i)
    {
        functional_units_.push_back(std::make_unique<divider>(arch.functional_units, &data_reg_file_));
    }
    for (auto i = 0U; i < arch.logic_unit_count; ++i)
    {
        functional_units_.push_back(std::make_unique<logic_fu>(arch.functional_units, &data_reg_file_));
    }
    for (auto i = 0U; i < arch.shifter_count; ++i)
    {
        functional_units_.push_back(std::make_unique<shifter>(arch.functional_units, &data_reg_file_));
    }
    for (auto i = 0U; i < arch.data_path_unit_count; ++i)
    {
        functional_units_.push_back(std::make_unique<data_path_unit>(arch.functional_units, &data_reg_file_));
    }
    for (auto i = 0U; i < arch.control_path_unit_count; ++i)
    {
        functional_units_.push_back(std::make_unique<control_path_unit>(arch.functional_units, &data_reg_file_));
    }
    
    data_reg_file_.set_architecture(arch.data_registers);
    control_reg_file_.set_architecture(arch.control_registers);
    branch_unit_.set_architecture(arch.branch);
    instruction_mem_.set_architecture(arch.memory);
}

void processing_element::reset()
{
    data_reg_file_.reset();
    control_reg_file_.reset();
    instruction_mem_.reset();
    branch_unit_.reset();

    for (auto &unit : functional_units_)
    {
        unit->reset();
    }
}

void processing_element::clock()
{
    if (active_)
    {
        instruction_mem_.clock(branch_unit_.pc());
        auto instruction = instruction_mem_.instruction();
        
		branch_unit::flag_register flags;
		unsigned int fu_index{ 0 }, flag_index{ 0 };
		for (auto &unit : functional_units_)
		{
			unit->clock(instruction.functional_instruction(fu_index++));
            for (auto i = 0U; i < unit->flagCount(); ++i)
            {
                flags.push_back(unit->flag(i));
            }
		}

		branch_unit_.flags(flags);
        branch_unit_.clock(instruction.branch_instruction());

        data_reg_file_.clock();
        control_reg_file_.clock();
    }
}

void processing_element::configure(const configuration& config)
{
	active_ = config.active;
    instruction_mem_.configure(config.program);
    data_reg_file_.configure(config.data_registers);
    control_reg_file_.configure(config.control_registers);
}

void processing_element::input_data(unsigned int index, data_t data)
{
    data_reg_file_.input_register(index).input(data);
}

void processing_element::input_control(unsigned int index, control_t data)
{
    control_reg_file_.input_register(index).input(data);
}

void processing_element::capture(state& s) const
{
    s.active = active_;
    s.program_counter = branch_unit_.pc();
    data_reg_file_.capture(s.data_registers);
    control_reg_file_.capture(s.control_registers);
}

}} // namespace tcpa::simulator
