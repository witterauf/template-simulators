#include "vliw.h"

namespace tcpa {
namespace simulator {

auto vliw::branch_instruction() const -> ::tcpa::simulator::branch_instruction
{
    return ::tcpa::simulator::branch_instruction{
        instruction_pointer_,
        arch_->functional_instruction_count * arch_->functional_instruction_width };
}

auto vliw::functional_instruction(unsigned int i) const -> ::tcpa::simulator::functional_instruction
{
    return ::tcpa::simulator::functional_instruction{
        instruction_pointer_, i * arch_->functional_instruction_width };
}

void vliw::raw_copy_to(uint8_t *destination) const
{
    const auto instruction_width = arch_->functional_instruction_count * arch_->functional_instruction_width + arch_->branch_instruction_width;
    for (auto i = 0U; i < in_full_bytes(instruction_width); ++i)
    {
        destination[i] = instruction_pointer_[i];
    }

    // zero out unneeded bits in last byte
    uint8_t mask = ((1 << (instruction_width & 7)) - 1);
    destination[in_full_bytes(instruction_width) - 1] &= mask;
}

auto instruction_word::read(const bit_field& field) const -> uint32_t
{
    return read_relative(field, 0);
}

auto instruction_word::read_relative(const bit_field& field, unsigned int offset) const -> uint32_t
{
    const uint64_t mask = (1ULL << field.width) - 1;

    auto position = field.offset + start_ + offset;
    auto byte_index = position >> 3;
    auto bit_index = position & 0b111;

    uint64_t aligned_bits{
        *reinterpret_cast<const uint64_t *>(&instruction_pointer_[byte_index]) >> bit_index
    };
    return static_cast<uint32_t>(aligned_bits & mask);
}

}} // namespace tcpa::simulator