#include "instruction_memory.h"

namespace tcpa {
namespace simulator {

instruction_memory::instruction_memory(const architecture& arch)
{
    read_adr_ = 0;
    set_architecture(arch);
}

void instruction_memory::set_architecture(const architecture& arch)
{
    instruction_width_ = arch.instruction_format.branch_instruction_width
        + arch.instruction_format.functional_instruction_count * arch.instruction_format.functional_instruction_width;
    memory_ = std::make_unique<uint8_t[]>(arch.size * in_full_bytes(instruction_width_));
    instruction_format_ = arch.instruction_format;
}

void instruction_memory::configure(instruction_adr_t address, vliw instruction)
{
    auto instruction_ptr = get_instruction_pointer(address);
    instruction.set_architecture(&instruction_format_);
    instruction.raw_copy_to(instruction_ptr);
}

void instruction_memory::configure(const configuration& config)
{
    for (auto i = 0U; i < config.vliws.size(); ++i)
    {
        configure(i, config.vliws[i]);
    }
}

void instruction_memory::reset()
{
    read_adr_ = 0;
}

void instruction_memory::clock(instruction_adr_t address)
{
    read_adr_ = address;
}

uint8_t *instruction_memory::get_instruction_pointer(instruction_adr_t address) const
{
	auto offset = address * in_full_bytes(instruction_width_);
    return memory_.get() + offset;
}

auto instruction_memory::instruction() const -> vliw
{
    return vliw{ const_cast<const uint8_t *>(get_instruction_pointer(read_adr_)), &instruction_format_ };
}

}} // namespace tcpa::simulator;