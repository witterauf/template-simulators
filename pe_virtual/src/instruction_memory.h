#pragma once

#include "bit_tools.h"
#include "vliw.h"
#include <cstdint>
#include <memory>
#include <vector>

namespace tcpa {
namespace simulator {

class instruction_memory
{
public:
    using instruction_adr_t = uint32_t;
    
    struct architecture
    {
        unsigned int size;
        vliw::architecture instruction_format;
    };
    struct configuration
    {
        std::vector<vliw> vliws;
    };

    instruction_memory() = default;
    instruction_memory(const architecture& arch);
    void set_architecture(const architecture& arch);

    void configure(const configuration& config);
    void configure(instruction_adr_t address, vliw instruction);

    void reset();
    void clock(instruction_adr_t address);

    auto instruction() const -> vliw;

private:
    uint8_t *get_instruction_pointer(instruction_adr_t address) const;

    instruction_adr_t read_adr_;
    std::unique_ptr<uint8_t[]> memory_;
    vliw::architecture instruction_format_;
    unsigned int instruction_width_;
};

}} // namespace tcpa::simulator