#pragma once

#include "instruction_memory.h"
#include "branch_unit.h"
#include "registers/register_file.h"
#include "functional_units/adder.h"
#include "functional_units/shifter.h"
#include "functional_units/multiplier.h"
#include "functional_units/divider.h"
#include "functional_units/logic_fu.h"
#include "functional_units/data_path_unit.h"
#include "functional_units/control_path_unit.h"
#include <array>

namespace tcpa {
namespace simulator {

class processing_element
{
public:
    struct architecture
    {
        unsigned int adder_count;
        unsigned int shifter_count;
        unsigned int multiplier_count;
        unsigned int divider_count;
        unsigned int logic_unit_count;
        unsigned int data_path_unit_count;
        unsigned int control_path_unit_count;

        instruction_memory::architecture memory;
        functional_unit::architecture functional_units;
        branch_unit::architecture branch;
        register_file::architecture data_registers;
        register_file::architecture control_registers;
    };
    struct configuration
    {
        bool active;
        register_file::configuration data_registers;
        register_file::configuration control_registers;
        instruction_memory::configuration program;
    };
    struct state
    {
        bool active;
        branch_unit::pc_type program_counter;
        register_file::state data_registers;
        register_file::state control_registers;
    };

    using data_t = uint32_t;
    using control_t = uint32_t;

    processing_element(const architecture& arch);
    void set_architecture(const architecture& arch);

    void reset();
    void clock();
    void configure(const configuration& config);
    void capture(state& s) const;

    void input_data(unsigned int index, data_t data);
    void input_control(unsigned int index, control_t data);

    // for debugging and testing
    /*
    instruction_memory& instruction_mem() { return instruction_mem_; }
    data_register_file& data_reg_file() { return data_reg_file_; }
    control_register_file& debug_get_control_reg_file() { return control_reg_file_; }
    branch_unit debug_get_branch_unit() { return branch_unit_; }
    */

private:
    bool active_ = false;
    register_file data_reg_file_;
    register_file control_reg_file_;
    instruction_memory instruction_mem_;
    branch_unit branch_unit_{ control_reg_file_ };
    std::vector<std::unique_ptr<functional_unit>> functional_units_;
};

}} // namespace tcpa::simulator
