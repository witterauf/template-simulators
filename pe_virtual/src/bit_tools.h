#pragma once

#include <cstdint>

namespace tcpa {
namespace simulator {

/// Calculates $\lfloor\operatorname{ld} x\rfloor$.
constexpr uint32_t ld(unsigned long long x)
{
	return x <= 1ULL ? 0ULL : 1ULL + ld(x >> 1ULL);
}

/// Calculates $\lceil\operatorname{ld} x\rceil$.
constexpr uint32_t number_of_bits(unsigned long long x)
{
	return x <= 1ULL ? 0ULL : 1ULL + ld(x - 1ULL);
}

/// Calculates $\lceil x/8\rceil$ (the number of bytes a bit string of length bits needs).
constexpr unsigned long long in_full_bytes(unsigned long long bits)
{
	return (bits >> 3) + ((bits & 0b111) != 0 ? 1 : 0);
}

struct bit_field
{
    unsigned int offset;
    unsigned int width;
};

inline uint32_t sign_extend(uint32_t value, unsigned int width)
{
	const uint32_t sign_bit = static_cast<uint32_t>(1U << (width - 1));
	const uint32_t mask = ~((1U << width) - 1);
	return static_cast<uint32_t>((value & sign_bit) != 0 ? (value | mask) : value);
}

}} // namespace tcpa::simulator