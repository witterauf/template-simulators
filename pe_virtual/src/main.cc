#include "processing_element.h"
#include <chrono>
#include <iostream>
#include <numeric>
#include <cmath>

template<class T>
auto calculate_median(const std::vector<T>& measurements)
    -> T
{
    auto m = measurements;
    std::nth_element(m.begin(), m.begin() + m.size() / 2, m.end());
    return m[m.size() / 2 ];
}

auto calculate_mean(const std::vector<std::chrono::nanoseconds>& measurements)
    -> std::chrono::nanoseconds
{
    std::chrono::nanoseconds sum(0);
    for (auto const& ns : measurements)
    {
        sum += ns;
    }
    return sum / measurements.size();
}

auto calculate_standard_deviation(const std::vector<std::chrono::nanoseconds>& measurements)
    -> std::chrono::duration<double, std::ratio<1, 1000000000ULL>>
{
    auto mean = calculate_mean(measurements);
    std::vector<uint64_t> diff(measurements.size(), 0);
    std::transform(measurements.begin(), measurements.end(), diff.begin(),
            [mean](std::chrono::nanoseconds x) {
                if (x >= mean)
                    return (x - mean).count() * (x - mean).count();
                else
                    return (mean - x).count() * (mean - x).count();
            });
    uint64_t sum = 0;
    for (auto const& d : diff)
    {
        sum += d;
    }
    auto dev_sqrt = std::sqrt((double)sum / (double)measurements.size());
    auto dev = std::chrono::duration<double, std::ratio<1, 1000000000ULL>>(dev_sqrt);
    return dev;
}

void measure_execution_time(tcpa::simulator::processing_element& simulator, const tcpa::simulator::processing_element::configuration& config)
{
    static constexpr auto number_of_executions = 1000U;
    static constexpr auto warm_up_executions = 100U;

    std::vector<std::chrono::nanoseconds> times;
    for (auto n = 0U; n < warm_up_executions + number_of_executions; ++n)
    {
        simulator.reset();
        simulator.configure(config);

        auto start = std::chrono::high_resolution_clock::now();
        for (auto i = 0U; i < 100000; ++i)
        {
            simulator.clock();
        }
        auto end = std::chrono::high_resolution_clock::now();
        times.push_back(std::chrono::nanoseconds(end - start));
    }

    times.erase(times.begin(), times.begin() + warm_up_executions);

    auto median = calculate_median(times);
    auto mean = calculate_mean(times);
    auto std_dev = calculate_standard_deviation(times);
    std::cout << "Median execution time for 10^5 cycles: "
        << std::chrono::duration<double, std::ratio<1, 1000>>(median).count() << " ms\n";
    std::cout << "Mean execution time for 10^5 cycles: "
        << std::chrono::duration<double, std::ratio<1, 1000>>(mean).count() << " +/-"
        << std::chrono::duration<double, std::ratio<1, 1000>>(std_dev).count() * 2.0 << " ms";
    std::cout << std::endl;
}

void measure_million_simulated_instructions(tcpa::simulator::processing_element& simulator, const tcpa::simulator::processing_element::configuration& config)
{
    static constexpr auto number_of_executions = 3U;
    static constexpr auto warm_up_executions = 1U;
    static constexpr auto time_limit = std::chrono::seconds(10);

    std::vector<uint64_t> instruction_counts;

    for (auto n = 0U; n < warm_up_executions + number_of_executions; ++n)
    {
        simulator.reset();
        simulator.configure(config);

        auto start = std::chrono::high_resolution_clock::now();
        auto end = std::chrono::high_resolution_clock::now();
        auto instructions = 0ULL;
        do
        {
            for (auto i = 0U; i < 1000; ++i)
            {
                simulator.clock();
            }
            instructions += 1000;
            end = std::chrono::high_resolution_clock::now();
        } while (std::chrono::duration_cast<std::chrono::seconds>(end - start) < time_limit);
        instruction_counts.push_back(instructions);
    }

    instruction_counts.erase(instruction_counts.begin(), instruction_counts.begin() + warm_up_executions);

    auto median = calculate_median(instruction_counts);
    std::cout << "Median million simulated instructions per second: "
        << (median / std::chrono::seconds(time_limit).count()) / 1000000.0 << "\n";
}

auto make_arch() -> tcpa::simulator::processing_element::architecture
{
    tcpa::simulator::processing_element::architecture arch = { 0 };

    arch.adder_count = 2;
    arch.multiplier_count = 2;
    arch.control_path_unit_count = 1;
    arch.data_path_unit_count = 1;

    arch.branch.condition_count = 1;
    arch.branch.target_width = 6;
    arch.branch.max_control_signals = 3;
    arch.branch.fu_types_with_flags = 5;
    arch.branch.max_fus_per_type = 4;
    arch.branch.max_flags_per_fu = 4;

    tcpa::simulator::data_register::architecture regArch;
    tcpa::simulator::fifo::architecture fifoArch;
    tcpa::simulator::feedback_shift_register::architecture feedbackArch;
    regArch.data_width = 1;
    fifoArch.max_length = 128;
    feedbackArch.max_length = 512;
    arch.control_registers.data_registers = std::vector<tcpa::simulator::data_register::architecture>(1, regArch);
    arch.control_registers.output_registers = std::vector<tcpa::simulator::data_register::architecture>(1, regArch);
    arch.control_registers.input_registers = std::vector<tcpa::simulator::fifo::architecture>(1, fifoArch);

    regArch.data_width = 32;
    arch.data_registers.data_registers = std::vector<tcpa::simulator::data_register::architecture>(16, regArch);
    arch.data_registers.output_registers = std::vector<tcpa::simulator::data_register::architecture>(5, regArch);
    arch.data_registers.input_registers = std::vector<tcpa::simulator::fifo::architecture>(5, fifoArch);
    arch.data_registers.feedback_shift_registers = std::vector<tcpa::simulator::feedback_shift_register::architecture>(3, feedbackArch);

    arch.functional_units.opcode_width = 3;
    arch.functional_units.instruction_width = 18;
    arch.functional_units.register_field_width = 5;
    arch.functional_units.immediate_width = 5;
    arch.functional_units.constant_width = 10;

    arch.memory.size = 64;
    arch.memory.instruction_format.branch_instruction_width = 1 * 9 + 2 * 6 + 3;
    arch.memory.instruction_format.functional_instruction_count = 6;
    arch.memory.instruction_format.functional_instruction_width = 18;
    return arch;
}

auto make_config() -> tcpa::simulator::processing_element::configuration
{
    tcpa::simulator::processing_element::configuration config;
    config.active = true;
    tcpa::simulator::fifo::configuration fifoConfig;
    tcpa::simulator::feedback_shift_register::configuration feedbackConfig;
    fifoConfig.length = 0;
    feedbackConfig.length = 1;
    config.control_registers.input_registers = std::vector<tcpa::simulator::fifo::configuration>(1, fifoConfig);
    config.control_registers.feedback_shift_registers = std::vector<tcpa::simulator::feedback_shift_register::configuration>(1, feedbackConfig);
    config.data_registers.input_registers = std::vector<tcpa::simulator::fifo::configuration>(5, fifoConfig);
    config.data_registers.feedback_shift_registers = std::vector<tcpa::simulator::feedback_shift_register::configuration>(3, feedbackConfig);
    return config;
}

int main()
{
    auto arch = make_arch();
    tcpa::simulator::processing_element pe(arch);
    auto config = make_config();
    measure_execution_time(pe, config);
    measure_million_simulated_instructions(pe, config);
    return 0;
}
