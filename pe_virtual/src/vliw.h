#pragma once

#include "bit_tools.h"
#include <cstdint>

namespace tcpa {
namespace simulator {

class instruction_word
{
public:
    instruction_word(const uint8_t *ip, unsigned int start)
        : instruction_pointer_{ ip }, start_{ start } {}

    auto read(const bit_field& field) const -> uint32_t;
    auto read_relative(const bit_field& filed, unsigned int offset) const -> uint32_t;

private:
    const uint8_t *instruction_pointer_;
    unsigned int start_;
};

using functional_instruction = instruction_word;
using branch_instruction = instruction_word;

class vliw
{
public:
    struct architecture
    {
        unsigned int branch_instruction_width;
        unsigned int functional_instruction_width;
        unsigned int functional_instruction_count;
    };

    vliw(const uint8_t *ip, const architecture* arch = nullptr)
        : instruction_pointer_{ ip }, arch_{ arch } {}
        
    void set_architecture(const architecture* arch) { arch_ = arch; }

    auto branch_instruction() const -> ::tcpa::simulator::branch_instruction;
    auto functional_instruction(unsigned int i) const -> ::tcpa::simulator::functional_instruction;

    void raw_copy_to(uint8_t *destination) const;

private:
    const architecture* arch_;
    const uint8_t *instruction_pointer_;
};

}} // namespace tcpa::simulator