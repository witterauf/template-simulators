#pragma once

#include "registers/register_file.h"
#include "bit_tools.h"
#include "vliw.h"
#include <array>

namespace tcpa {
namespace simulator {

class branch_unit
{
public:
    using pc_type = uint32_t;
    
    struct architecture
    {
        unsigned int condition_count;
        unsigned int target_width;
        unsigned int fu_types_with_flags;
        unsigned int max_fus_per_type;
        unsigned int max_flags_per_fu;
        unsigned int max_control_signals;
    };

    using flag_register = std::vector<bool>;

    branch_unit(register_file& registers)
        : control_reg_file_{ registers } {}        
    void set_architecture(const architecture& arch);

    void reset();
    void clock(branch_instruction instruction);

    void flags(const flag_register &flags) { flags_ = flags; }

    pc_type pc() const { return pc_; }

private:
    void do_branch(branch_instruction instruction);
    void do_next(branch_instruction instruction);

    bool check_condition(branch_instruction instruction, unsigned int index);
    bool check_fu_flag(branch_instruction instruction, unsigned int index, unsigned int fu_type) const;
    bool check_control_signal(branch_instruction instruction, unsigned int index);

    auto read_from_condition(const bit_field& field, branch_instruction instruction, unsigned int index) const -> uint32_t;

    bit_field opcode_;
    bit_field target_;
    struct
    {
        bit_field fu_type;
        bit_field fu_index;
        bit_field flag_index;
        bit_field control_signal_index;
    } condition_;
    
    unsigned int instruction_width_;
    unsigned int condition_width_;
    unsigned int target_count_;
    architecture arch_;
    
    register_file& control_reg_file_;
	flag_register flags_ = { false };
    pc_type pc_;
};

}} // namespace tcpa::simulator