Example: Cycle-accurate Simulator of a Light-weight VLIW Processing Element
###########################################################################

This is a companion repository for a paper describing how to construct cycle-accurate, fast simulators for acceleratings in C++ using templates.
Since this is just a proof of concept it is a bit messy, but using CMake, it should be easy to compiler and experiment with.
