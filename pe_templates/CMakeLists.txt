cmake_minimum_required(VERSION 3.1)
project(pe-templates)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -fpermissive")
endif()

add_library(pe-sim-templates
    src/vliw.h
    src/instruction_memory.h
    src/branch_unit.h
    src/processing_element.h
    src/pe_sim.h
    src/pe_sim.cc
)
target_include_directories(pe-sim-templates PUBLIC src/)

add_executable(pe-sim-templates-test
    src/main.cc
)
target_link_libraries(pe-sim-templates-test pe-sim-templates)
