#ifndef __TCPASIM_PROCESSOR_INSTRUCTION_MEMORY__
#define __TCPASIM_PROCESSOR_INSTRUCTION_MEMORY__

#include "bit_tools.h"
#include "vliw.h"
#include <cstdint>
#include <memory>
#include <vector>

namespace tcpa {
namespace simulator {

template<typename Architecture, class VLIW>
class instruction_memory
{
public:
    using vliw = VLIW;
    using instruction_adr_t = typename Architecture::instruction_adr_t;

    static constexpr auto instruction_width = vliw::instruction_width;
    static constexpr auto size = Architecture::size;

    struct configuration
    {
        using vliw = VLIW;

        std::vector<vliw> vliws;
    };

    instruction_memory();

    void configure(const configuration& config);
    void configure(instruction_adr_t address, vliw instruction);

    void reset();
    void do_clock(instruction_adr_t address);

    vliw instruction() const;

private:
    uint8_t *get_instruction_pointer(instruction_adr_t address) const;

    instruction_adr_t read_adr_;
    std::unique_ptr<uint8_t[]> memory_;
};

//##[ implementation ]#############################################################################

template<typename Architecture, class VLIW>
instruction_memory<Architecture, VLIW>::instruction_memory()
{
    read_adr_ = 0;
    memory_ = std::make_unique<uint8_t[]>(size * in_full_bytes(instruction_width));
}

template<typename Architecture, class VLIW>
void instruction_memory<Architecture, VLIW>::configure(instruction_adr_t address, vliw instruction)
{
    auto instruction_ptr = get_instruction_pointer(address);
    instruction.raw_copy_to(instruction_ptr);
}

template<typename Architecture, class VLIW>
void instruction_memory<Architecture, VLIW>::configure(const configuration& config)
{
    if (config.vliws.size() > size)
        throw std::runtime_error{ "[instruction_memory::configure(...)] number of VLIWs exceeds size of instruction memory" };

    for (auto i = 0U; i < config.vliws.size(); ++i)
    {
        configure(i, config.vliws[i]);
    }
}

template<typename Architecture, class VLIW>
void instruction_memory<Architecture, VLIW>::reset()
{
    read_adr_ = 0;
}

template<typename Architecture, class VLIW>
void instruction_memory<Architecture, VLIW>::do_clock(instruction_adr_t address)
{
    read_adr_ = address;
}

template<typename Architecture, class VLIW>
uint8_t *instruction_memory<Architecture, VLIW>::get_instruction_pointer(
        instruction_adr_t address) const
{
	auto offset = address * in_full_bytes(instruction_width);
    return memory_.get() + offset;
}

template<typename Architecture, class VLIW>
auto instruction_memory<Architecture, VLIW>::instruction() const -> vliw
{
    return vliw{ const_cast<const uint8_t *>(get_instruction_pointer(read_adr_)) };
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_INSTRUCTION_MEMORY__
