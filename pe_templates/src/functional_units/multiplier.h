#ifndef __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_MULTIPLIER__
#define __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_MULTIPLIER__

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

template<typename Architecture, class RegisterFile>
class multiplier: public functional_unit<Architecture, RegisterFile>
{
public:
    using functional_unit = functional_unit<Architecture, RegisterFile>;

    using data_t = typename functional_unit::data_t;

	static constexpr unsigned int opcode_mult = 0b000;
	static constexpr unsigned int opcode_multi = 0b001;
	static constexpr unsigned int opcode_mucst = 0b010;
	static constexpr unsigned int opcode_nop = 0b111;

    void reset() {}
    void do_clock(functional_instruction instruction);

private:
    void do_mult(functional_instruction instruction);
    void do_multi(functional_instruction instruction);
    void do_mucst(functional_instruction instruction);

    // necessary because templated class inherits from templated class
    using functional_unit::get_opcode;
    using functional_unit::do_three_registers;
    using functional_unit::do_immediate;
    using functional_unit::do_constant;
};

//##[ implementation ]#############################################################################

template<typename Architecture, class RegisterFile>
void multiplier<Architecture, RegisterFile>::do_clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_mult: do_mult(instruction); break;
        case opcode_multi: do_multi(instruction); break;
        case opcode_mucst: do_mucst(instruction); break;
        case opcode_nop: default: break;
    }
}

template<typename Architecture, class RegisterFile>
void multiplier<Architecture, RegisterFile>::do_mult(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a * b; });
}

template<typename Architecture, class RegisterFile>
void multiplier<Architecture, RegisterFile>::do_multi(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a * b; });
}

template<typename Architecture, class RegisterFile>
void multiplier<Architecture, RegisterFile>::do_mucst(functional_instruction instruction)
{
    do_constant(instruction);
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_MULTIPLIER__
