#ifndef __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_DIVIDER__
#define __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_DIVIDER__

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

template<typename Architecture, class RegisterFile>
class divider: public functional_unit<Architecture, RegisterFile>
{
public:
    using functional_unit = functional_unit<Architecture, RegisterFile>;
    using data_t = typename functional_unit::data_t;

	static constexpr unsigned int opcode_div = 0b000;
	static constexpr unsigned int opcode_nop = 0b111;

    void reset() {}
    void do_clock(functional_instruction instruction);

private:
    void do_div(functional_instruction instruction);

    // necessary because templated class inherits from templated class
    using functional_unit::get_opcode;
    using functional_unit::do_three_registers;
};

//##[ implementation ]#############################################################################

template<typename Architecture, class RegisterFile>
void divider<Architecture, RegisterFile>::do_clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_div: do_div(instruction); break;
        case opcode_nop: default: break;
    }
}

template<typename Architecture, class RegisterFile>
void divider<Architecture, RegisterFile>::do_div(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a / b; });
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_DIVIDER__
