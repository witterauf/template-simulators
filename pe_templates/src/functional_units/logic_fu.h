#ifndef __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_LOGIC_FU__
#define __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_LOGIC_FU__

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

template<typename Architecture, class RegisterFile>
class logic_fu: public functional_unit<Architecture, RegisterFile>
{
public:
    using functional_unit = functional_unit<Architecture, RegisterFile>;
    using data_t = typename functional_unit::data_t;

	static constexpr unsigned int opcode_and = 0b000;
	static constexpr unsigned int opcode_or = 0b001;
	static constexpr unsigned int opcode_xor = 0b010;
	static constexpr unsigned int opcode_neg = 0b011;
	static constexpr unsigned int opcode_andi = 0b100;
	static constexpr unsigned int opcode_ori = 0b101;
	static constexpr unsigned int opcode_xori = 0b110;
	static constexpr unsigned int opcode_nop = 0b111;

    void reset() {}
    void do_clock(functional_instruction instruction);

private:
    void do_and(functional_instruction instruction);
    void do_or(functional_instruction instruction);
    void do_xor(functional_instruction instruction);
    void do_neg(functional_instruction instruction);
    void do_andi(functional_instruction instruction);
    void do_ori(functional_instruction instruction);
    void do_xori(functional_instruction instruction);

    // necessary because templated class inherits from templated class
    // (C++...)
    using functional_unit::get_opcode;
    using functional_unit::do_three_registers;
    using functional_unit::do_two_registers;
    using functional_unit::do_immediate;
};

//##[ implementation ]#############################################################################

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_and: do_and(instruction); break;
        case opcode_or: do_or(instruction); break;
        case opcode_xor: do_xor(instruction); break;
        case opcode_neg: do_neg(instruction); break;
        case opcode_andi: do_andi(instruction); break;
        case opcode_ori: do_ori(instruction); break;
        case opcode_xori: do_xori(instruction); break;
        case opcode_nop: default: break;
    }
}

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_and(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a & b; });
}

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_or(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a | b; });
}

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_xor(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a ^ b; });
}

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_neg(functional_instruction instruction)
{
    do_two_registers(instruction, [](data_t val){ return ~val; });
}

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_andi(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a & b; });
}

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_ori(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a | b; });
}

template<typename Architecture, class RegisterFile>
void logic_fu<Architecture, RegisterFile>::do_xori(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a ^ b; });
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_LOGIC_FU__
