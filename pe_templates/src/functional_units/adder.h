#ifndef __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_ADDER__
#define __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_ADDER__

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

template<typename Architecture, class RegisterFile>
class adder: public functional_unit<Architecture, RegisterFile>
{
public:
    using functional_unit = functional_unit<Architecture, RegisterFile>;
    using data_t = typename functional_unit::data_t;

    static constexpr unsigned int opcode_add = 0b000;
    static constexpr unsigned int opcode_sub = 0b001;
    static constexpr unsigned int opcode_addi = 0b010;
    static constexpr unsigned int opcode_subi = 0b011;
    static constexpr unsigned int opcode_const = 0b100;
    static constexpr unsigned int opcode_nop = 0b111;

    struct flags_t
    {
        bool zero;
        bool overflow;
        bool negative;
        bool carry;
    };

    using functional_unit::set_reg_file;

    void reset();
    void do_clock(functional_instruction instruction);

    const flags_t &flags() const { return flags_; }

private:
    data_t add(data_t a, data_t b); 
    data_t sub(data_t a, data_t b); 

    void do_add(functional_instruction instruction);
    void do_addi(functional_instruction instruction);
    void do_sub(functional_instruction instruction);
    void do_subi(functional_instruction instruction);
    void do_const(functional_instruction instruction);

    // necessary because templated class inherits from templated class
    using functional_unit::get_opcode;
    using functional_unit::do_constant;
    using functional_unit::do_three_registers;
    using functional_unit::do_immediate;

    flags_t flags_;
};

//##[ implementation ] ########################################################

template<typename Architecture, class RegisterFile>
void adder<Architecture, RegisterFile>::reset() {}

template<typename Architecture, class RegisterFile>
void adder<Architecture, RegisterFile>::do_clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_add: do_add(instruction); break;
        case opcode_sub: do_sub(instruction); break;
        case opcode_addi: do_addi(instruction); break;
        case opcode_subi: do_subi(instruction); break;
        case opcode_const: do_const(instruction); break;
        case opcode_nop: default: flags_ = {}; break;
    }
}

template<typename Architecture, class RegisterFile>
void adder<Architecture, RegisterFile>::do_add(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->add(a, b); });
}

template<typename Architecture, class RegisterFile>
void adder<Architecture, RegisterFile>::do_addi(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->add(a, b); });
}

template<typename Architecture, class RegisterFile>
auto adder<Architecture, RegisterFile>::add(data_t a, data_t b) -> data_t
{
    data_t result = a + b;
    flags_.zero = result == 0;
    flags_.negative = static_cast<typename std::make_signed<data_t>::type>(result) < 0;
    flags_.carry = static_cast<typename std::make_unsigned<data_t>::type>(result)
                        < static_cast<typename std::make_unsigned<data_t>::type>(a);
    flags_.overflow = ((~(a ^ b) & (result ^ a)) >> (sizeof(data_t) * 8 - 1)) != 0;
    return result;
}

template<typename Architecture, class RegisterFile>
void adder<Architecture, RegisterFile>::do_sub(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->sub(a, b); });
}

template<typename Architecture, class RegisterFile>
void adder<Architecture, RegisterFile>::do_subi(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->sub(a, b); });
}

template<typename Architecture, class RegisterFile>
auto adder<Architecture, RegisterFile>::sub(data_t a, data_t b) -> data_t
{
    return add(a, ~b + 1);
}

template<typename Architecture, class RegisterFile>
void adder<Architecture, RegisterFile>::do_const(functional_instruction instruction)
{
    do_constant(instruction);
    flags_ = {};
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_ADDER__
