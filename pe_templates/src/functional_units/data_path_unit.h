#ifndef __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_DATA_PATH_UNIT__
#define __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_DATA_PATH_UNIT__

#include "functional_unit.h"

namespace tcpa {
namespace simulator {

template<typename Architecture, class RegisterFile>
class data_path_unit: public functional_unit<Architecture, RegisterFile>
{
public:
    using functional_unit = functional_unit<Architecture, RegisterFile>;
    using data_t = typename functional_unit::data_t;

	static constexpr unsigned int opcode_move = 0b000;
	static constexpr unsigned int opcode_const = 0b001;
	static constexpr unsigned int opcode_nop = 0b111;

    void reset() {}
    void do_clock(functional_instruction instruction);

private:
    void do_move(functional_instruction instruction);
    void do_const(functional_instruction instruction);

    // necessary because templated class inherits from templated class
    // (C++...)
    using functional_unit::get_opcode;
    using functional_unit::do_two_registers;
    using functional_unit::do_constant;
};

//##[ implementation ]#############################################################################

template<typename Architecture, class RegisterFile>
void data_path_unit<Architecture, RegisterFile>::do_clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_move: do_move(instruction); break;
        case opcode_const: do_const(instruction); break;
        case opcode_nop: default: break;
    }
}

template<typename Architecture, class RegisterFile>
void data_path_unit<Architecture, RegisterFile>::do_move(functional_instruction instruction)
{
    do_two_registers(instruction, [](data_t val){ return val; });
}

template<typename Architecture, class RegisterFile>
void data_path_unit<Architecture, RegisterFile>::do_const(functional_instruction instruction)
{
    do_constant(instruction);
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_DATA_PATH_UNIT__
