#ifndef __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_SHIFTER__
#define __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_SHIFTER__

#include "functional_unit.h"
#include <type_traits>

namespace tcpa {
namespace simulator {

template<typename Architecture, class RegisterFile>
class shifter: public functional_unit<Architecture, RegisterFile>
{
public:
    using functional_unit = functional_unit<Architecture, RegisterFile>;

    using data_t = typename functional_unit::data_t;
    using data_t_unsigned = typename std::make_unsigned<data_t>::type;
    using data_t_signed = typename std::make_signed<data_t>::type;

	static constexpr unsigned int opcode_shl = 0b000;
	static constexpr unsigned int opcode_shr = 0b001;
	static constexpr unsigned int opcode_ashr = 0b010;
	static constexpr unsigned int opcode_shli = 0b011;
	static constexpr unsigned int opcode_shri = 0b100;
	static constexpr unsigned int opcode_ashri = 0b101;
	static constexpr unsigned int opcode_nop = 0b111;

    void reset() {}
    void do_clock(functional_instruction instruction);

private:
    void do_shl(functional_instruction instruction);
    void do_shr(functional_instruction instruction);
    void do_ashr(functional_instruction instruction);
    void do_shli(functional_instruction instruction);
    void do_shri(functional_instruction instruction);
    void do_ashri(functional_instruction instruction);

    data_t do_shr(data_t a, data_t b) const;
    data_t do_ashr(data_t a, data_t b) const;

    // necessary because templated class inherits from templated class
    using functional_unit::get_opcode;
    using functional_unit::do_three_registers;
    using functional_unit::do_immediate;
};

//##[ implementation ]#############################################################################

template<typename Architecture, class RegisterFile>
void shifter<Architecture, RegisterFile>::do_clock(functional_instruction instruction)
{
    auto op = get_opcode(instruction);
    switch (op)
    {
        case opcode_shl: do_shl(instruction); break;
        case opcode_shr: do_shr(instruction); break;
        case opcode_ashr: do_ashr(instruction); break;
        case opcode_shli: do_shli(instruction); break;
        case opcode_shri: do_shri(instruction); break;
        case opcode_ashri: do_ashri(instruction); break;
        case opcode_nop: default: break;
    }
}

template<typename Architecture, class RegisterFile>
void shifter<Architecture, RegisterFile>::do_shl(functional_instruction instruction)
{
    do_three_registers(instruction, [](data_t a, data_t b){ return a << b; });
}

template<typename Architecture, class RegisterFile>
void shifter<Architecture, RegisterFile>::do_shr(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->do_shr(a, b); });
}

template<typename Architecture, class RegisterFile>
void shifter<Architecture, RegisterFile>::do_ashr(functional_instruction instruction)
{
    do_three_registers(instruction, [&](data_t a, data_t b){ return this->do_ashr(a, b); });
}

template<typename Architecture, class RegisterFile>
void shifter<Architecture, RegisterFile>::do_shli(functional_instruction instruction)
{
    do_immediate(instruction, [](data_t a, data_t b){ return a << b; });
}

template<typename Architecture, class RegisterFile>
void shifter<Architecture, RegisterFile>::do_shri(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->do_shr(a, b); });
}

template<typename Architecture, class RegisterFile>
void shifter<Architecture, RegisterFile>::do_ashri(functional_instruction instruction)
{
    do_immediate(instruction, [&](data_t a, data_t b){ return this->do_ashr(a, b); });
}

template<typename Architecture, class RegisterFile>
auto shifter<Architecture, RegisterFile>::do_shr(data_t a, data_t b) const -> data_t
{
    return static_cast<data_t_unsigned>(a) >> static_cast<data_t_unsigned>(b);
}

template<typename Architecture, class RegisterFile>
auto shifter<Architecture, RegisterFile>::do_ashr(data_t a, data_t b) const -> data_t
{
    return static_cast<data_t_signed>(a) >> b;
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_FUNCTIONAL_UNITS_SHIFTER__
