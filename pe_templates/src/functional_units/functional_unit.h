#ifndef __TCPASIM_PROCESSOR_FUNCTIONAL_UNIT__
#define __TCPASIM_PROCESSOR_FUNCTIONAL_UNIT__

#include "bit_tools.h"
#include "vliw.h"

namespace tcpa {
namespace simulator {

template<typename Architecture, class RegisterFile>
class functional_unit
{
public:
    using data_t = typename RegisterFile::data_t;
    static constexpr unsigned int instruction_width = Architecture::instruction_width;
    static constexpr unsigned int opcode_width = Architecture::opcode_width;
    static constexpr unsigned int register_width = Architecture::register_width;
    static constexpr unsigned int constant_width = Architecture::constant_width;
    static constexpr unsigned int immediate_width = Architecture::immediate_width;

    // setter instead of constructor:
    // functional_units are stored in std::arrays, meaning they must have
    // a default constructor
    void set_reg_file(RegisterFile* reg_file) { reg_file_ = reg_file; }

protected:
    // instruction formats
    using opcode = bit_field<opcode_width, instruction_width - opcode_width>;

    using three_regs_destination = bit_field<register_width, opcode::start - register_width>;
    using three_regs_source_a = bit_field<register_width, three_regs_destination::start - register_width>;
    using three_regs_source_b = bit_field<register_width, three_regs_source_a::start - register_width>;
    static_assert(instruction_width >= opcode::width
            + three_regs_destination::width + three_regs_source_a::width + three_regs_source_b::width,
            "functional_unit: instruction width is not large enough for three registers");

    using two_regs_destination = bit_field<register_width, opcode::start - register_width>;
    using two_regs_source = bit_field<register_width, two_regs_destination::start - register_width>;
    static_assert(instruction_width >= opcode::width
            + two_regs_destination::width + two_regs_source::width,
            "functional_unit: instruction width is not large enough for three registers");

    using constant_destination = three_regs_destination;
    using constant_value = bit_field<constant_width, constant_destination::start - constant_width>;
    static_assert(instruction_width >= opcode::width
            + constant_destination::width + constant_value::width,
            "functional_unit: instruction width is not large enough for constant loading");

    using immediate_destination = three_regs_destination;
    using immediate_source_reg = three_regs_source_a;
    using immediate_value = bit_field<immediate_width, immediate_source_reg::start - immediate_width>;
    static_assert(instruction_width >= opcode::width +
            immediate_destination::width + immediate_source_reg::width + immediate_value::width,
            "functional_unit: instruction width is not large enough for immediate arguments");

    auto get_opcode(functional_instruction instruction) const -> typename opcode::value_type;

    // predefined operation types
    void do_constant(functional_instruction instruction);
    template<typename Operation>
    void do_three_registers(functional_instruction instruction, Operation op);
    template<typename Operation>
    void do_two_registers(functional_instruction instruction, Operation op);
	template<typename Operation>
    void do_immediate(functional_instruction instruction, Operation op);

    RegisterFile* reg_file_ = nullptr;
};

//##[ implementation ]#########################################################

template<typename Architecture, class RegisterFile>
auto functional_unit<Architecture, RegisterFile>::get_opcode(functional_instruction instruction) const
    -> typename opcode::value_type
{
    return instruction.read<opcode>();
}

template<typename Architecture, class RegisterFile>
template<typename Operation>
void functional_unit<Architecture, RegisterFile>::do_three_registers(functional_instruction instruction, Operation op)
{
    auto destination = instruction.read<three_regs_destination>();
    auto source_a = instruction.read<three_regs_source_a>();
    auto source_b = instruction.read<three_regs_source_b>();
    auto value_a = reg_file_->read_by_cumulative_index(source_a);
    auto value_b = reg_file_->read_by_cumulative_index(source_b);
    auto result = op(value_a, value_b);
    reg_file_->write_by_cumulative_index(destination, result);
}

template<typename Architecture, class RegisterFile>
template<typename Operation>
void functional_unit<Architecture, RegisterFile>::do_two_registers(functional_instruction instruction, Operation op)
{
    auto destination = instruction.read<two_regs_destination>();
    auto source = instruction.read<two_regs_source>();
    auto value = reg_file_->read_by_cumulative_index(source);
    auto result = op(value);
    reg_file_->write_by_cumulative_index(destination, result);
}

template<typename Architecture, class RegisterFile>
template<typename Operation>
void functional_unit<Architecture, RegisterFile>::do_immediate(functional_instruction instruction, Operation op)
{
    auto destination = instruction.read<immediate_destination>();
    auto source_a = instruction.read<immediate_source_reg>();
    auto value_b = instruction.read<immediate_value>();
    auto value_a = reg_file_->read_by_cumulative_index(source_a);
    auto result = op(value_a, value_b);
    reg_file_->write_by_cumulative_index(destination, result);
}

template<typename Architecture, class RegisterFile>
void functional_unit<Architecture, RegisterFile>::do_constant(functional_instruction instruction)
{
    auto destination = instruction.read<constant_destination>();
    auto constant = instruction.read<constant_value>();
    reg_file_->write_by_cumulative_index(destination, sign_extend<constant_value::width>(constant));
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_FUNCTIONAL_UNIT__
