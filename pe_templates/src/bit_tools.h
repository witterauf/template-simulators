#pragma once

namespace tcpa {
namespace simulator {

/// Calculates $\lfloor\operatorname{ld} x\rfloor$.
constexpr unsigned long long ld(unsigned long long x)
{
	return x <= 1ULL ? 0ULL : 1ULL + ld(x >> 1ULL);
}

/// Calculates $\lceil\operatorname{ld} x\rceil$.
constexpr unsigned long long number_of_bits(unsigned long long x)
{
	return x <= 1ULL ? 0ULL : 1ULL + ld(x - 1ULL);
}

/// Calculates $\lceil x/8\rceil$ (the number of bytes a bit string of length bits needs).
constexpr unsigned long long in_full_bytes(unsigned long long bits)
{
	return (bits >> 3) + ((bits & 0b111) != 0 ? 1 : 0);
}

template<unsigned Width, unsigned Pos, typename T = unsigned>
struct bit_field
{
	using value_type = T;
	static constexpr unsigned int width = Width;
	static constexpr unsigned int start = Pos;
	static constexpr unsigned int end = start + width;
};

template<unsigned FromWidth, typename T>
T sign_extend(T value)
{
	static_assert(FromWidth >= 1, "[signExtend()] FromWidth must be at least 1");
	constexpr T sign_bit = static_cast<T>(1ULL << (FromWidth - 1));
	constexpr unsigned long long mask = ~((1ULL << FromWidth) - 1);
	return static_cast<T>((value & sign_bit) != 0 ? (value | mask) : value);
}

}} // namespace tcpa::simulator