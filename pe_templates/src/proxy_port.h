#ifndef __TCPASIM_INTERCONNECT_PROXY_PORT__
#define __TCPASIM_INTERCONNECT_PROXY_PORT__

namespace tcpa {
namespace simulator {

template<typename T>
class proxy_port
{
public:
    proxy_port() = default;
    proxy_port(const T* source): source_{ source } {}
    T read() const { return source_ ? *source_ : throw 1; }
    bool has_source() const { return source_ != nullptr; }

    T operator*() const { return *source_; }
    operator bool() const { return source_ != nullptr; };

private:
    const T* source_ = nullptr;
};

}} // namespace tcpa::simulator

#endif // __TCPASIM_INTERCONNECT_PROXY_PORT__
