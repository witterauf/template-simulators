#pragma once

#include "instruction_memory.h"
#include "branch_unit.h"
#include "registers/data_register_file.h"
#include "registers/control_register_file.h"
#include "functional_units/adder.h"
#include "functional_units/shifter.h"
#include "functional_units/multiplier.h"
#include "functional_units/divider.h"
#include "functional_units/logic_fu.h"
#include "functional_units/data_path_unit.h"
#include "functional_units/control_path_unit.h"
#include <array>

namespace tcpa {
namespace simulator {

template<typename Architecture>
class processing_element
{
public:
    static constexpr auto number_of_adders = Architecture::number_of_adders;
    static constexpr auto number_of_multipliers = Architecture::number_of_multipliers;
    static constexpr auto number_of_dividers = Architecture::number_of_dividers;
    static constexpr auto number_of_logic_units = Architecture::number_of_logic_units;
    static constexpr auto number_of_shifters = Architecture::number_of_shifters;
    static constexpr auto number_of_data_path_units = Architecture::number_of_data_path_units;
    static constexpr auto number_of_control_path_units = Architecture::number_of_control_path_units;

    using data_t = typename Architecture::data_t;
    using control_t = typename Architecture::control_t;

    using data_register_file = data_register_file<typename Architecture::registers>;
    using control_register_file = control_register_file<typename Architecture::control_registers>;

    using vliw = vliw<typename Architecture::instruction_format>;
    using instruction_memory = instruction_memory<typename Architecture::instruction_memory, vliw>;

    // for convenience ordered according to position in VLIW
    using adder = adder<typename Architecture::functional_unit, data_register_file>;
    using multiplier = multiplier<typename Architecture::functional_unit, data_register_file>;
    using divider = divider<typename Architecture::functional_unit, data_register_file>;
    using logic_fu = logic_fu<typename Architecture::functional_unit, data_register_file>;
    using shifter = shifter<typename Architecture::functional_unit, data_register_file>;
    using data_path_unit = data_path_unit<typename Architecture::functional_unit, data_register_file>;
    using control_path_unit = control_path_unit<typename Architecture::functional_unit, control_register_file>;

    using branch_unit = branch_unit<typename Architecture::branch_unit, control_register_file>;

    using data_proxy_port = typename data_register<data_t>::proxy_port;
    using control_proxy_port = typename data_register<control_t>::proxy_port;

    struct configuration
    {
        using program_configuration = typename instruction_memory::configuration;
        using data_register_configuration = typename data_register_file::configuration;
        using control_register_configuration = typename control_register_file::configuration;

        bool active;
        program_configuration program;
        data_register_configuration data_registers;
        control_register_configuration control_registers;
    };

    struct state
    {
		bool active;
        typename branch_unit::pc_type program_counter;
        typename data_register_file::state data_registers;
        typename control_register_file::state control_registers;
    };

public:
    processing_element();

    void reset();
    void do_clock();
    void configure(const configuration& config);
    void capture(state& s) const;

    void input_data(unsigned int index, data_t data);
    void input_control(unsigned int index, control_t data);

    data_proxy_port data_output_proxy(unsigned int index) const;
    control_proxy_port control_output_proxy(unsigned int index) const;

    // for debugging and testing
    instruction_memory& instruction_mem() { return instruction_mem_; }
    data_register_file& data_reg_file() { return data_reg_file_; }
    control_register_file& debug_get_control_reg_file() { return control_reg_file_; }
    branch_unit debug_get_branch_unit() { return branch_unit_; }

private:
    bool active_ = false;
    data_register_file data_reg_file_;
    control_register_file control_reg_file_;
    instruction_memory instruction_mem_;
    branch_unit branch_unit_{ control_reg_file_ };
    std::array<adder, number_of_adders> adders_;
    std::array<multiplier, number_of_multipliers> multipliers_;
    std::array<divider, number_of_dividers> dividers_;
    std::array<logic_fu, number_of_logic_units> logic_units_;
    std::array<shifter, number_of_shifters> shifters_;
    std::array<data_path_unit, number_of_data_path_units> dpus_;
    std::array<control_path_unit, number_of_control_path_units> cpus_;
};

//##[ implementation ]#############################################################################

template<typename Architecture>
processing_element<Architecture>::processing_element()
{
    for (auto& adder : adders_)
        adder.set_reg_file(&data_reg_file_);
    for (auto& multiplier : multipliers_)
        multiplier.set_reg_file(&data_reg_file_);
    for (auto& divider : dividers_)
        divider.set_reg_file(&data_reg_file_);
    for (auto& logic_unit : logic_units_)
        logic_unit.set_reg_file(&data_reg_file_);
    for (auto& shifter : shifters_)
        shifter.set_reg_file(&data_reg_file_);
    for (auto& dpu : dpus_)
        dpu.set_reg_file(&data_reg_file_);
    for (auto& cpu : cpus_)
        cpu.set_reg_file(&control_reg_file_);
}

template<typename Architecture>
void processing_element<Architecture>::reset()
{
    data_reg_file_.reset();
    control_reg_file_.reset();
    instruction_mem_.reset();
    branch_unit_.reset();

    for (auto &adder : adders_)
        adder.reset();
    for (auto &multiplier : multipliers_)
        multiplier.reset();
    for (auto &divider : dividers_)
        divider.reset();
    for (auto &logic_unit : logic_units_)
        logic_unit.reset();
    for (auto &shifter : shifters_)
        shifter.reset();
    for (auto &dpu : dpus_)
        dpu.reset();
    for (auto &cpu : cpus_)
        cpu.reset();
}

// right now, everything is done within 1 cycle
template<typename Architecture>
void processing_element<Architecture>::do_clock()
{
    if (active_)
    {
        instruction_mem_.do_clock(branch_unit_.pc());
        auto instruction = instruction_mem_.instruction();
		typename branch_unit::flag_register flags;

		unsigned int fu_index{ 0 }, flag_index{ 0 };
		for (auto &adder : adders_)
		{
			adder.do_clock(instruction.functional_instruction(fu_index++));
			flags[flag_index++] = adder.flags().zero;
			flags[flag_index++] = adder.flags().overflow;
			flags[flag_index++] = adder.flags().negative;
			flags[flag_index++] = adder.flags().carry;
		}
        for (auto &multiplier : multipliers_)
            multiplier.do_clock(instruction.functional_instruction(fu_index++));
        for (auto &divider : dividers_)
            divider.do_clock(instruction.functional_instruction(fu_index++));
        for (auto &logic_unit : logic_units_)
            logic_unit.do_clock(instruction.functional_instruction(fu_index++));
        for (auto &shifter : shifters_)
            shifter.do_clock(instruction.functional_instruction(fu_index++));
        for (auto i = 0U; i < number_of_data_path_units; ++i)
            dpus_[i].do_clock(instruction.functional_instruction(fu_index++));
        for (auto i = 0U; i < number_of_control_path_units; ++i)
            cpus_[i].do_clock(instruction.functional_instruction(fu_index++));

		branch_unit_.flags(flags);
        branch_unit_.do_clock(instruction.branch_instruction());

        data_reg_file_.do_clock();
        control_reg_file_.do_clock();
    }
}

template<typename Architecture>
void processing_element<Architecture>::configure(const configuration& config)
{
	active_ = config.active;
    instruction_mem_.configure(config.program);
    data_reg_file_.configure(config.data_registers);
    control_reg_file_.configure(config.control_registers);
}

template<typename Architecture>
void processing_element<Architecture>::input_data(unsigned int index, data_t data)
{
    data_reg_file_.id(index).input(data);
}

template<typename Architecture>
void processing_element<Architecture>::input_control(unsigned int index, control_t data)
{
    control_reg_file_.ic(index).input(data);
}

template<typename Architecture>
auto processing_element<Architecture>::data_output_proxy(unsigned int index) const
    -> data_proxy_port
{
    return data_reg_file_.od(index).generate_proxy();
}

template<typename Architecture>
auto processing_element<Architecture>::control_output_proxy(unsigned int index) const
    -> control_proxy_port
{
    return control_reg_file_.oc(index).generate_proxy();
}

template<typename Architecture>
void processing_element<Architecture>::capture(state& s) const
{
	s.active = active_;
    s.program_counter = branch_unit_.pc();
    data_reg_file_.capture(s.data_registers);
    control_reg_file_.capture(s.control_registers);
}

}} // namespace tcpa::simulator