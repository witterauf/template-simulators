#ifndef __TCPASIM_UTILITIES_SHIFT_REGISTER__
#define __TCPASIM_UTILITIES_SHIFT_REGISTER__

#include <algorithm>
#include <array>
#include <stdexcept>
#include <vector>

namespace tcpa {
namespace simulator {

// length can be reconfigured at runtime, maximum length is fixed
template<typename T, unsigned int MaxLength>
class shift_register
{
public:
    static constexpr auto max_length = MaxLength;
    using value_type = T;
    using state = std::vector<T>;

    void reset(value_type initial_value = value_type{});
    void do_clock();
    void capture(state& s) const;

    void length(size_t length);

    void input(value_type signal);
    value_type output() const;

private:
    std::array<T, MaxLength + 1> values_;
    size_t length_;
    size_t position_;
};

template<typename T, unsigned int MaxLength>
void shift_register<T, MaxLength>::reset(value_type initial_value)
{
    std::fill_n(values_.begin(), max_length, initial_value);
    position_ = 0;
}

template<typename T, unsigned int MaxLength>
void shift_register<T, MaxLength>::length(size_t length)
{
    if (length > max_length)
        throw std::runtime_error{ "[shift_register::length(length)] length exceeds max_length" };

    length_ = length;
}

template<typename T, unsigned int MaxLength>
void shift_register<T, MaxLength>::do_clock()
{
    if (length_ > 0)
        position_ = (position_ + 1) % (max_length + 1);
}

template<typename T, unsigned int MaxLength>
void shift_register<T, MaxLength>::input(value_type signal)
{
    values_[(position_ + length_) % (max_length + 1)] = signal;
}

template<typename T, unsigned int MaxLength>
auto shift_register<T, MaxLength>::output() const -> value_type
{
    return values_[position_];
}

template<typename T, unsigned int MaxLength>
void shift_register<T, MaxLength>::capture(state& s) const
{
    s.clear();
	for (auto i = 0U; i <= length_; ++i)
	{
		s.push_back(values_[(position_ + i) % (max_length + 1)]);
	}
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_UTILITIES_SHIFT_REGISTER__
