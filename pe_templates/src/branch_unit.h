#ifndef __TCPASIM_PROCESSOR_BRANCH_UNIT__
#define __TCPASIM_PROCESSOR_BRANCH_UNIT__

#include "bit_tools.h"
#include "vliw.h"
#include <array>

namespace tcpa {
namespace simulator {

template<typename Architecture, class ControlRegisterFile>
class branch_unit
{
public:
    using pc_type = typename Architecture::pc_type;

    static constexpr auto number_of_conditions = Architecture::number_of_conditions;
    static constexpr auto target_width = Architecture::target_width;
    static constexpr auto fu_types_with_flags = Architecture::fu_types_with_flags;
    static constexpr auto max_fus_per_type = Architecture::max_fus_per_type;
    static constexpr auto max_flags_per_fu = Architecture::max_flags_per_fu;
    static constexpr auto max_control_signals = Architecture::max_control_signals;

    static constexpr auto max_flags = fu_types_with_flags * max_fus_per_type * max_flags_per_fu;
    static constexpr auto fu_type_width = number_of_bits(fu_types_with_flags + 1);
    static constexpr auto fu_index_width = number_of_bits(max_fus_per_type);
    static constexpr auto flag_index_width = number_of_bits(max_flags_per_fu);
    static constexpr auto control_signal_index_width = number_of_bits(max_control_signals);

    static constexpr auto condition_width = fu_type_width
        + fu_index_width + flag_index_width + control_signal_index_width;

    static constexpr auto number_of_targets = 1 << number_of_conditions;

    static constexpr unsigned int opcode_width = 3;
    static constexpr unsigned int opcode_branch = 0b000;
    static constexpr unsigned int opcode_next = 0b111;

    static constexpr auto instruction_width =
        opcode_width
        + number_of_conditions * condition_width
        + number_of_targets * target_width;

    using flag_register = std::array<bool, max_flags>;

    branch_unit(ControlRegisterFile &control_reg_file)
        : control_reg_file_{ control_reg_file } {}

    void reset();
    void do_clock(branch_instruction instruction);

    void flags(const flag_register &flags) { flags_ = flags; }

    pc_type pc() const { return pc_; }

private:
    // BRANCH_INSTRUCTION := OPCODE | CONDITION | ... | CONDITION | TARGET | ... | TARGET
    // CONDITION := FU_TYPE | FU_INDEX | FLAG_INDEX | CONTROL_SIGNAL_INDEX
    using opcode = bit_field<3, instruction_width - 3>;

    using condition_fu_type = bit_field<fu_type_width, condition_width - fu_type_width>;
    using condition_fu_index = bit_field<fu_index_width, condition_fu_type::start - fu_index_width>;
    using condition_flag_index =
        bit_field<flag_index_width, condition_fu_index::start - flag_index_width>;
    using condition_control_signal_index =
        bit_field<control_signal_index_width, 0>;

    using target = bit_field<target_width, 0>;
    
    void do_branch(branch_instruction instruction);
    void do_next(branch_instruction instruction);

    bool check_condition(branch_instruction instruction, unsigned int index);
    bool check_fu_flag(branch_instruction instruction, unsigned int index, unsigned int fu_type) const;
    bool check_control_signal(branch_instruction instruction, unsigned int index);

    template<typename BitField>
    auto read_from_condition(branch_instruction instruction, unsigned int index) const
        -> typename BitField::value_type;

    ControlRegisterFile &control_reg_file_;
	flag_register flags_ = {};
    pc_type pc_;
};

//##[ implementation ]#############################################################################

template<typename Architecture, class ControlRegisterFile>
void branch_unit<Architecture, ControlRegisterFile>::reset()
{
    pc_ = 0;
}

template<typename Architecture, class ControlRegisterFile>
void branch_unit<Architecture, ControlRegisterFile>::do_clock(branch_instruction instruction)
{
    auto op = instruction.read<opcode>();
    switch (op)
    {
        case opcode_branch: do_branch(instruction); break;
        case opcode_next:
        default: do_next(instruction); break;
    }
}

template<typename Architecture, class ControlRegisterFile>
void branch_unit<Architecture, ControlRegisterFile>::do_branch(branch_instruction instruction)
{
    auto target_index = 0U;
    for (auto i = 0U; i < number_of_conditions; ++i)
    {
        target_index += check_condition(instruction, i) ? (1 << i) : 0;
    }
    pc_ = instruction.read_relative<target>(target_index * target_width);
}

template<typename Architecture, class ControlRegisterFile>
void branch_unit<Architecture, ControlRegisterFile>::do_next(branch_instruction)
{
    pc_ += 1;
}

template<typename Architecture, class ControlRegisterFile>
bool branch_unit<Architecture, ControlRegisterFile>::check_condition(
        branch_instruction instruction, unsigned int index)
{
    static constexpr unsigned int control_signal = 0b111;

    auto fu_type = read_from_condition<condition_fu_type>(instruction, index);
    if (fu_type == control_signal)
    {
        return check_control_signal(instruction, index);
    }
    else
    {
        return check_fu_flag(instruction, index, fu_type);
    }
}

template<typename Architecture, class ControlRegisterFile>
bool branch_unit<Architecture, ControlRegisterFile>::check_fu_flag(
        branch_instruction instruction, unsigned int index, unsigned int fu_type) const
{
    auto fu_index = read_from_condition<condition_fu_index>(instruction, index);
    auto flag_index = read_from_condition<condition_flag_index>(instruction, index);
    auto overall_index = fu_type * max_fus_per_type * max_flags_per_fu
        + fu_index * max_flags_per_fu + flag_index;
    return flags_[overall_index];
}

template<typename Architecture, class ControlRegisterFile>
bool branch_unit<Architecture, ControlRegisterFile>::check_control_signal(
        branch_instruction instruction, unsigned int index)
{
    auto control_signal_index = read_from_condition<condition_control_signal_index>(instruction, index);
    auto control_signal_value = control_reg_file_.read_by_cumulative_index(control_signal_index);
    return (control_signal_value & 1) != 0;
}

template<typename Architecture, class ControlRegisterFile>
template<typename BitField>
auto branch_unit<Architecture, ControlRegisterFile>::read_from_condition(
        branch_instruction instruction, unsigned int index) const -> typename BitField::value_type
{
    return instruction.read_relative<BitField>(opcode::start - (index + 1) * condition_width);
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_BRANCH_UNIT__
