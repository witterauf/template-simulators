#include "pe_sim.h"
#include <chrono>
#include <iostream>
#include <numeric>
#include <cmath>

template<class T>
auto calculate_median(const std::vector<T>& measurements)
    -> T
{
    auto m = measurements;
    std::nth_element(m.begin(), m.begin() + m.size() / 2, m.end());
    return m[m.size() / 2 ];
}

auto calculate_mean(const std::vector<std::chrono::nanoseconds>& measurements)
    -> std::chrono::nanoseconds
{
    std::chrono::nanoseconds sum(0);
    for (auto const& ns : measurements)
    {
        sum += ns;
    }
    return sum / measurements.size();
}

auto calculate_standard_deviation(const std::vector<std::chrono::nanoseconds>& measurements)
    -> std::chrono::duration<double, std::ratio<1, 1000000000ULL>>
{
    auto mean = calculate_mean(measurements);
    std::vector<uint64_t> diff(measurements.size(), 0);
    std::transform(measurements.begin(), measurements.end(), diff.begin(),
            [mean](std::chrono::nanoseconds x) {
                if (x >= mean)
                    return (x - mean).count() * (x - mean).count();
                else
                    return (mean - x).count() * (mean - x).count();
            });
    uint64_t sum = 0;
    for (auto const& d : diff)
    {
        sum += d;
    }
    auto dev_sqrt = std::sqrt((double)sum / (double)measurements.size());
    auto dev = std::chrono::duration<double, std::ratio<1, 1000000000ULL>>(dev_sqrt);
    return dev;
}

void measure_execution_time(pe_sim_templates& simulator, const pe_sim_templates::configuration& config)
{
    static constexpr auto number_of_executions = 1000U;
    static constexpr auto warm_up_executions = 100U;

    std::vector<std::chrono::nanoseconds> times;
    for (auto n = 0U; n < warm_up_executions + number_of_executions; ++n)
    {
        simulator.reset();
        simulator.configure(config);

        auto start = std::chrono::high_resolution_clock::now();
        for (auto i = 0U; i < 100000; ++i)
        {
            simulator.do_clock();
        }
        auto end = std::chrono::high_resolution_clock::now();
        times.push_back(std::chrono::nanoseconds(end - start));
    }

    times.erase(times.begin(), times.begin() + warm_up_executions);

    auto median = calculate_median(times);
    auto mean = calculate_mean(times);
    auto std_dev = calculate_standard_deviation(times);
    std::cout << "Median execution time for 10^5 cycles: "
        << std::chrono::duration<double, std::ratio<1, 1000>>(median).count() << " ms\n";
    std::cout << "Mean execution time for 10^5 cycles: "
        << std::chrono::duration<double, std::ratio<1, 1000>>(mean).count() << " +/-"
        << std::chrono::duration<double, std::ratio<1, 1000>>(std_dev).count() * 2.0 << " ms";
    std::cout << std::endl;
}

void measure_million_simulated_instructions(pe_sim_templates& simulator, const pe_sim_templates::configuration& config)
{
    static constexpr auto number_of_executions = 3U;
    static constexpr auto warm_up_executions = 1U;
    static constexpr auto time_limit = std::chrono::seconds(10);

    std::vector<uint64_t> instruction_counts;

    for (auto n = 0U; n < warm_up_executions + number_of_executions; ++n)
    {
        simulator.reset();
        simulator.configure(config);

        auto start = std::chrono::high_resolution_clock::now();
        auto end = std::chrono::high_resolution_clock::now();
        auto instructions = 0ULL;
        do
        {
            for (auto i = 0U; i < 1000; ++i)
            {
                simulator.do_clock();
            }
            instructions += 1000;
            end = std::chrono::high_resolution_clock::now();
        } while (std::chrono::duration_cast<std::chrono::seconds>(end - start) < time_limit);
        instruction_counts.push_back(instructions);
    }

    instruction_counts.erase(instruction_counts.begin(), instruction_counts.begin() + warm_up_executions);

    auto median = calculate_median(instruction_counts);
    std::cout << "Median million simulated instructions per second: "
        << (median / std::chrono::seconds(time_limit).count()) / 1000000.0 << "\n";
}

auto make_config()
    -> pe_sim_templates::configuration
{
    pe_sim_templates::configuration config;
    config.active = true;
    config.control_registers.input_fifo_lengths[0] = 0;
    config.data_registers.input_fifo_lengths[0] = 0;
    config.data_registers.input_fifo_lengths[1] = 0;
    config.data_registers.input_fifo_lengths[2] = 0;
    config.data_registers.input_fifo_lengths[3] = 0;
    config.data_registers.input_fifo_lengths[4] = 0;
    config.data_registers.feedback_shift_lengths[0] = 0;
    config.data_registers.feedback_shift_lengths[1] = 0;
    config.data_registers.feedback_shift_lengths[2] = 0;
    return config;
}

int main()
{
    pe_sim_templates simulator;
    auto config = make_config();
    measure_execution_time(simulator, config);
    measure_million_simulated_instructions(simulator, config);
    return 0;
}
