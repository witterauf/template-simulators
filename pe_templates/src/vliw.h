#ifndef __TCPASIM_PROCESSOR_INSTRUCTION_WORDS_
#define __TCPASIM_PROCESSOR_INSTRUCTION_WORDS_

#include <cstdint>

namespace tcpa {
namespace simulator {

class instruction_word
{
public:
    instruction_word(const uint8_t *ip, unsigned int start)
        : instruction_pointer_{ ip }, start_{ start } {}

    template<class BitField>
    auto read() const -> typename BitField::value_type;
    template<class BitField>
    auto read_relative(unsigned int offset) const -> typename BitField::value_type;

private:
    const uint8_t *instruction_pointer_;
    unsigned int start_;
};

using functional_instruction = instruction_word;
using branch_instruction = instruction_word;

template<typename InstructionFormat>
class vliw
{
public:
    static constexpr auto branch_instruction_width = InstructionFormat::branch_instruction_width;
    static constexpr auto functional_instruction_width = InstructionFormat::functional_instruction_width;
    static constexpr auto number_of_functional_instructions = InstructionFormat::number_of_functional_instructions;
    static constexpr auto instruction_width
        = branch_instruction_width + number_of_functional_instructions * functional_instruction_width;

    vliw(const uint8_t *ip)
        : instruction_pointer_{ ip } {}

    branch_instruction branch_instruction() const;
    functional_instruction functional_instruction(unsigned int i) const;

    void raw_copy_to(uint8_t *destination) const;

private:
    const uint8_t *instruction_pointer_;
};

//##[ implementation ]#############################################################################

template<typename InstructionFormat>
branch_instruction vliw<InstructionFormat>::branch_instruction() const
{
    return ::tcpa::simulator::branch_instruction{ instruction_pointer_,
        number_of_functional_instructions * functional_instruction_width };
}

template<typename InstructionFormat>
functional_instruction vliw<InstructionFormat>::functional_instruction(unsigned int i) const
{
    return ::tcpa::simulator::functional_instruction{
        instruction_pointer_, static_cast<unsigned int>(i * functional_instruction_width) };
}

template<typename InstructionFormat>
void vliw<InstructionFormat>::raw_copy_to(uint8_t *destination) const
{
    for (auto i = 0U; i < in_full_bytes(instruction_width); ++i)
    {
        destination[i] = instruction_pointer_[i];
    }

    // zero out unneeded bits in last byte
    uint8_t mask = ((1 << (instruction_width & 7)) - 1);
    destination[in_full_bytes(instruction_width) - 1] &= mask;
}

template<class BitField>
auto instruction_word::read() const -> typename BitField::value_type
{
    return read_relative<BitField>(0);
}

template<class BitField>
auto instruction_word::read_relative(unsigned int offset) const
    -> typename BitField::value_type
{
    static_assert(BitField::width <= 57,
            "[instruction_word::read<BitField>()] only works up to width of 57 bits!");
    static constexpr uint64_t mask = (1ULL << BitField::width) - 1;

    auto position = BitField::start + start_ + offset;
    auto byte_index = position >> 3;
    auto bit_index = position & 0b111;

    uint64_t aligned_bits{
        *reinterpret_cast<const uint64_t *>(&instruction_pointer_[byte_index]) >> bit_index
    };
    return static_cast<typename BitField::value_type>(aligned_bits & mask);
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_INSTRUCTION_WORDS_
