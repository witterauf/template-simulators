#include "processing_element.h"
#include "bit_tools.h"

using tcpa::simulator::number_of_bits;

constexpr unsigned long long max(unsigned long long a, unsigned long long b)
{
    return a >= b ? a : b;
}

struct Architecture
{
    static constexpr auto number_of_adders = 2U;
    static constexpr auto number_of_multipliers = 2U;
    static constexpr auto number_of_dividers = 0U;
    static constexpr auto number_of_logic_units = 0U;
    static constexpr auto number_of_shifters = 0U;
    static constexpr auto number_of_data_path_units = 1U;
    static constexpr auto number_of_control_path_units = 1U;

    using data_t = uint32_t;
    using control_t = uint32_t;

    struct registers
    {
        using data_t = data_t;

        static constexpr auto nr_of_data_regs = 16;
        static constexpr auto nr_of_output_regs = 5;
        static constexpr auto nr_of_input_regs = 5;
        static constexpr auto nr_of_feedback_shift_regs = 3;
        static constexpr auto register_count = nr_of_data_regs + nr_of_output_regs + nr_of_input_regs + nr_of_feedback_shift_regs;
        static constexpr auto max_fifo_length = 128;
        static constexpr auto max_feedback_length = 512;
    };

    struct control_registers
    {
        using control_t = control_t;

        static constexpr auto nr_of_data_regs = 1;
        static constexpr auto nr_of_output_regs = 1;
        static constexpr auto nr_of_input_regs = 1;
        static constexpr auto nr_of_feedback_shift_regs = 0;
        static constexpr auto register_count = nr_of_data_regs + nr_of_output_regs + nr_of_input_regs + nr_of_feedback_shift_regs;
        static constexpr auto max_fifo_length = 128;
        static constexpr auto max_feedback_length = 512;
    };

    struct functional_unit
    {
        static constexpr auto opcode_width = 18;
        static constexpr auto register_width = number_of_bits(max(registers::register_count, control_registers::register_count));
        static constexpr auto control_register_width = control_registers::register_count;
        static constexpr auto instruction_width = opcode_width + 3 * register_width;
        static constexpr auto constant_width = instruction_width - register_width - opcode_width;
        static constexpr auto control_constant_width = instruction_width - control_register_width - opcode_width;
        static constexpr auto immediate_width = instruction_width - 2 * register_width - opcode_width;
    };

    struct instruction_memory
    {
        using instruction_adr_t = uint32_t;
        static constexpr auto size = 64;
    };

    struct branch_unit
    {
        using pc_type = typename instruction_memory::instruction_adr_t;

        static constexpr auto number_of_conditions = 1;
        static constexpr auto target_width = 6;
        static constexpr auto fu_types_with_flags = 5;
        static constexpr auto max_fus_per_type = 4;
        static constexpr auto max_flags_per_fu = 4;
        static constexpr auto max_control_signals = 3;

        static_assert(number_of_conditions > 0, "Branch unit must have at least one branch condition");
        static_assert(fu_types_with_flags > 0, "Branch unit must support at least one functional unit type");
        static_assert(max_fus_per_type > 0, "Branch unit must support at least one functional unit per type");
        static_assert(max_flags_per_fu > 0, "Branch unit must support at least one flag per functional unit");
        static_assert(max_control_signals > 0, "Branch unit must support at least one control signal");

        static constexpr auto number_of_targets = 1 << number_of_conditions;
        static constexpr auto condition_width = number_of_bits(fu_types_with_flags) + number_of_bits(max_fus_per_type)
            + number_of_bits(max_flags_per_fu) + number_of_bits(max_control_signals);
        static constexpr auto instruction_width = 3 + number_of_conditions * condition_width + number_of_targets * target_width;
    };

    struct instruction_format
    {
        static constexpr auto branch_instruction_width = branch_unit::instruction_width;
        static constexpr auto functional_instruction_width = functional_unit::instruction_width;
        static constexpr auto number_of_functional_instructions = number_of_adders + number_of_multipliers
            + number_of_dividers + number_of_logic_units + number_of_shifters + number_of_data_path_units + number_of_control_path_units;
    };
};

extern template class tcpa::simulator::processing_element<Architecture>;
using pe_sim_templates = tcpa::simulator::processing_element<Architecture>;
