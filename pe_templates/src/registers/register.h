#ifndef __TCPASIM_PROCESSOR_REGISTERS_REGISTER__
#define __TCPASIM_PROCESSOR_REGISTERS_REGISTER__

#include "proxy_port.h"

namespace tcpa {
namespace simulator {

template<typename T>
class data_register
{
public:
		using value_type = T;
		using data_t = T;
		using state = data_t;
		using proxy_port = proxy_port<T>;

		void reset() { input_ = 0; }
		void do_clock() { value_ = input_; }
		void capture(state& s) const { s = value_; }

		void input(value_type val) { input_ = val; }
		value_type output() const { return value_; }

		proxy_port generate_proxy() const { return proxy_port{ &value_ }; }

		void force(value_type val) { input_ = value_ = val; }

private:
		value_type input_ = 0;
		value_type value_ = 0;
};

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_REGISTERS_REGISTER__
