#ifndef __TCPASIM_PROCESSOR_REGISTERS_FEEDBACK_SHIFT_REGISTER__
#define __TCPASIM_PROCESSOR_REGISTERS_FEEDBACK_SHIFT_REGISTER__

#include <algorithm>
#include <array>
#include <iterator>
#include <stdexcept>
#include <vector>

namespace tcpa {
namespace simulator {

template<typename T, size_t MaximumLength>
class feedback_shift_register
{
public:
    using value_type = T;
    using ref = T&;
    using const_ref = value_type const&;

    using state = std::vector<T>;

	static constexpr unsigned int max_length = MaximumLength;

public:
	feedback_shift_register(size_t length = 1)
        : length_{ length } {}

	void do_clock(bool write, bool read);
    void reset(value_type initial_value = value_type{});
    void capture(state& s) const;

    void length(size_t length);
	size_t length() const { return length_; }

    void input(value_type input) { input_ = input; }
    value_type output() const { return fifo_[position_]; }

private:
	value_type input_ = 0;

    std::array<value_type, max_length> fifo_;
	unsigned int position_ = 0;
	size_t length_ = 1;
};

//##[ implementation ]#########################################################

template<typename T, size_t MaximumLength>
void feedback_shift_register<T, MaximumLength>::reset(value_type initial_value)
{
    std::fill_n(fifo_.begin(), max_length, initial_value);
}

template<typename T, size_t MaximumLength>
void feedback_shift_register<T, MaximumLength>::do_clock(bool write, bool read)
{
    if (write)
        fifo_[position_] = input_;
    if (read || write)
        position_ = (position_ + 1) % length_;
}

template<typename T, size_t MaximumLength>
void feedback_shift_register<T, MaximumLength>::length(size_t length)
{
    //if (length < 1)
        //throw std::runtime_error{ "[feedback_shift_register::length(size_t)] length must be at least 1" };
    if (length < 1) length = 1;
    if (length > max_length)
        throw std::runtime_error{ "[feedback_shift_register::length(size_t)] length must not exceed maximum" };
    length_ = length;
}

template<typename T, size_t MaximumLength>
void feedback_shift_register<T, MaximumLength>::capture(state& s) const
{
    s.clear();
    std::copy(fifo_.begin(), fifo_.begin() + length_, std::back_inserter(s));
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_REGISTERS_FEEDBACK_SHIFT_REGISTER__
