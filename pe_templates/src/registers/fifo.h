#ifndef __TCPASIM_PROCESSOR_REGISTERS_FIFO__
#define __TCPASIM_PROCESSOR_REGISTERS_FIFO__

#include "shift_register.h"

namespace tcpa {
namespace simulator {

template<typename T, size_t MaxLength>
class fifo
{
public:
    using value_type = T;
    using state = typename shift_register<T, MaxLength>::state;

    static constexpr auto max_length = MaxLength;

public:
    void reset() { sr_.reset(); }
    void do_clock() {sr_.do_clock(); }
    void capture(state& s) const { sr_.capture(s); }

    void length(size_t length)
	{
		// 1 cycle additional delay
		sr_.length(length + 1);
	}

    void input(value_type value) { sr_.input(value); }
    value_type output() const { return sr_.output(); }

private:
    shift_register<value_type, max_length> sr_;
};

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_REGISTERS_FIFO__
