#ifndef __TCPASIM_PROCESSOR_REGISTERS_REGISTER_FILE__
#define __TCPASIM_PROCESSOR_REGISTERS_REGISTER_FILE__

#include "register.h"
#include "fifo.h"
#include "feedback_shift_register.h"
#include <stdexcept>
#include <array>

namespace tcpa {
namespace simulator {

template<typename Architecture>
class register_file
{
public:
    using data_t = typename Architecture::data_t;
	using feedback_shift_register = feedback_shift_register<data_t, Architecture::max_feedback_length>;
	using data_register = data_register<data_t>;
	using fifo_register = fifo<data_t, Architecture::max_fifo_length>;

    static constexpr auto nr_of_data_regs = Architecture::nr_of_data_regs;
    static constexpr auto nr_of_output_regs = Architecture::nr_of_output_regs;
    static constexpr auto nr_of_input_regs = Architecture::nr_of_input_regs;
    static constexpr auto nr_of_feedback_shift_regs = Architecture::nr_of_feedback_shift_regs;

    static constexpr auto id_of_first_data_reg = 0;
    static constexpr auto id_of_first_output_reg = id_of_first_data_reg + nr_of_data_regs;
    static constexpr auto id_of_first_input_reg = id_of_first_output_reg + nr_of_output_regs;
    static constexpr auto id_of_first_feedback_shift_reg = id_of_first_input_reg + nr_of_input_regs;

    struct configuration
    {
        std::array<unsigned int, nr_of_input_regs> input_fifo_lengths;
        std::array<unsigned int, nr_of_feedback_shift_regs> feedback_shift_lengths;
    };

    struct state
    {
        std::array<typename data_register::state, nr_of_data_regs> data_registers;
        std::array<typename data_register::state, nr_of_output_regs> output_registers;
        std::array<typename fifo_register::state, nr_of_input_regs> input_registers;
        std::array<typename feedback_shift_register::state, nr_of_feedback_shift_regs> feedback_shift_registers;
    };

public:
    void reset();
    void do_clock();
    void configure(const configuration& config);
    void capture(state& s) const;

    // high-level interface
    void write_by_cumulative_index(unsigned int i, data_t value);
    data_t read_by_cumulative_index(unsigned int i);

    // sub-component access
	data_register& data_reg(unsigned int i) { return data_regs_[i]; }
	const data_register& data_reg(unsigned int i) const { return data_regs_[i]; }
	data_register& output_reg(unsigned int i) { return output_regs_[i]; }
	const data_register& output_reg(unsigned int i) const { return output_regs_[i]; }
	fifo_register& input_reg(unsigned int i) { return input_regs_[i]; }
	const fifo_register& input_reg(unsigned int i) const { return input_regs_[i]; }
	feedback_shift_register& feedback_shift_reg(unsigned int i)
    {
        return feedback_shift_ports_[i].feedback_shift_reg;
    }
	const feedback_shift_register& feedback_shift_reg(unsigned int i) const
    {
        return feedback_shift_ports_[i].feedback_shift_reg;
    }

    bool is_feedback_shift_reg(unsigned int i) const;
    bool is_input_reg(unsigned int i) const;
    bool is_output_reg(unsigned int i) const;
    bool is_data_reg(unsigned int i) const;

private:
    struct feedback_shift_port
    {
        bool was_read;
        bool was_written;
        feedback_shift_register feedback_shift_reg;
    };

    data_t feedback_shift_read(unsigned int i);
    void feedback_shift_write(unsigned int, data_t data);

    std::array<fifo_register, nr_of_input_regs> input_regs_;
    std::array<data_register, nr_of_data_regs> data_regs_;
    std::array<data_register, nr_of_output_regs> output_regs_;
    std::array<feedback_shift_port, nr_of_feedback_shift_regs> feedback_shift_ports_;
};

//##[ implementation ]#############################################################################

template<typename Architecture>
void register_file<Architecture>::reset()
{
    for (auto &input_reg : input_regs_)
        input_reg.reset();
    for (auto &data_reg : data_regs_)
        data_reg.reset();
    for (auto &output_reg : output_regs_)
        output_reg.reset();

    for (auto &fs_port : feedback_shift_ports_)
    {
        fs_port.feedback_shift_reg.reset();
        fs_port.was_read = fs_port.was_written = false;
    }
}

template<typename Architecture>
void register_file<Architecture>::do_clock()
{
    for (auto &input_reg : input_regs_)
        input_reg.do_clock();
    for (auto &data_reg : data_regs_)
        data_reg.do_clock();
    for (auto &output_reg : output_regs_)
        output_reg.do_clock();

    for (auto &fs_port : feedback_shift_ports_)
    {
        fs_port.feedback_shift_reg.do_clock(fs_port.was_written, fs_port.was_read);
        fs_port.was_read = fs_port.was_written = false;
    }
}

template<typename Architecture>
void register_file<Architecture>::configure(const configuration& config)
{
    for (auto i = 0U; i < nr_of_input_regs; ++i)
    {
        input_regs_[i].length(config.input_fifo_lengths[i]);
    } 
    for (auto i = 0U; i < nr_of_feedback_shift_regs; ++i)
    {
        feedback_shift_ports_[i].feedback_shift_reg.length(config.feedback_shift_lengths[i]);
    } 
}

template<typename Architecture>
auto register_file<Architecture>::read_by_cumulative_index(unsigned int i) -> data_t
{
    if (is_feedback_shift_reg(i))
        return feedback_shift_read(i - id_of_first_feedback_shift_reg);
	else if (is_input_reg(i))
		return input_regs_[i - id_of_first_input_reg].output();
	else if (is_output_reg(i))
		return output_regs_[i - id_of_first_output_reg].output();
	else if (is_data_reg(i))
        return data_regs_[i - id_of_first_data_reg].output();
    else
        throw std::runtime_error{ "[register_file::read_by_cumulative_index(...)] invalid register index" };
}

template<typename Architecture>
auto register_file<Architecture>::feedback_shift_read(unsigned int i) -> data_t
{
    feedback_shift_ports_[i].was_read = true;
    return feedback_shift_ports_[i].feedback_shift_reg.output();
}

template<typename Architecture>
void register_file<Architecture>::write_by_cumulative_index(unsigned int i, data_t value)
{
    if (is_feedback_shift_reg(i))
        feedback_shift_write(i - id_of_first_feedback_shift_reg, value);
	else if (is_input_reg(i))
		input_regs_[i - id_of_first_input_reg].input(value);
	else if (is_output_reg(i))
		output_regs_[i - id_of_first_output_reg].input(value);
	else if (is_data_reg(i))
        data_regs_[i - id_of_first_data_reg].input(value);
    else
        throw std::runtime_error{ "[register_file::write_by_cumulative_index(...)] invalid register index" };
}

template<typename Architecture>
void register_file<Architecture>::feedback_shift_write(unsigned int i, data_t data)
{
    feedback_shift_ports_[i].was_written = true;
    feedback_shift_ports_[i].feedback_shift_reg.input(data);
}

template<typename Architecture>
bool register_file<Architecture>::is_feedback_shift_reg(unsigned int i) const
{
    return nr_of_feedback_shift_regs >= 0
        && i >= id_of_first_feedback_shift_reg;
}

template<typename Architecture>
bool register_file<Architecture>::is_input_reg(unsigned int i) const
{
    return nr_of_input_regs >= 0
        && i >= id_of_first_input_reg
        && i < id_of_first_feedback_shift_reg;
}

template<typename Architecture>
bool register_file<Architecture>::is_output_reg(unsigned int i) const
{
    return nr_of_output_regs >= 0
        && i >= id_of_first_output_reg
        && i < id_of_first_input_reg;
}

template<typename Architecture>
bool register_file<Architecture>::is_data_reg(unsigned int i) const
{
    return nr_of_data_regs >= 0
        && i >= id_of_first_data_reg
        && i < id_of_first_output_reg;
}

template<typename Architecture>
void register_file<Architecture>::capture(state& s) const
{
	for (auto i = 0U; i < nr_of_data_regs; ++i)
	{
		data_regs_[i].capture(s.data_registers[i]);
	}
	for (auto i = 0U; i < nr_of_input_regs; ++i)
	{
		input_regs_[i].capture(s.input_registers[i]);
	}
	for (auto i = 0U; i < nr_of_output_regs; ++i)
	{
		output_regs_[i].capture(s.output_registers[i]);
	}
	for (auto i = 0U; i < nr_of_feedback_shift_regs; ++i)
	{
		feedback_shift_ports_[i].feedback_shift_reg.capture(s.feedback_shift_registers[i]);
	}
}

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_REGISTERS_REGISTER_FILE__
