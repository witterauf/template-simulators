#ifndef __TCPASIM_PROCESSOR_REGISTERS_DATA_REGISTER_FILE__
#define __TCPASIM_PROCESSOR_REGISTERS_DATA_REGISTER_FILE__

#include "register_file.h"

namespace tcpa {
namespace simulator {

template<typename Architecture>
struct data_register_architecture
{
public:
    using data_t = typename Architecture::data_t;

    static constexpr auto nr_of_data_regs = Architecture::nr_of_data_regs;
    static constexpr auto nr_of_input_regs = Architecture::nr_of_input_regs;
    static constexpr auto nr_of_output_regs = Architecture::nr_of_output_regs;
    static constexpr auto nr_of_feedback_shift_regs = Architecture::nr_of_feedback_shift_regs;
    static constexpr auto max_fifo_length = Architecture::max_fifo_length;
    static constexpr auto max_feedback_length = Architecture::max_fifo_length;
};

template<typename Architecture>
class data_register_file: public register_file<data_register_architecture<Architecture>>
{
public:
    using base = register_file<data_register_architecture<Architecture>>;
    using data_t = typename base::data_t;
    using feedback_shift_register = typename base::feedback_shift_register;
    using data_register = typename base::data_register;
    using fifo_register = typename base::fifo_register;

    static const auto nr_of_rds = base::nr_of_data_regs;
    static const auto nr_of_ids = base::nr_of_input_regs;
    static const auto nr_of_ods = base::nr_of_output_regs;
    static const auto nr_of_fds = base::nr_of_feedback_shift_regs;

	data_register& rd(unsigned int i) { return data_reg(i); }
	const data_register& rd(unsigned int i) const { return data_reg(i); }
	data_register& od(unsigned int i) { return output_reg(i); }
	const data_register& od(unsigned int i) const { return output_reg(i); }
	fifo_register& id(unsigned int i) { return input_reg(i); }
	const fifo_register& id(unsigned int i) const { return input_reg(i); }
	feedback_shift_register& fd(unsigned int i) { return feedback_shift_reg(i); }
	const feedback_shift_register& fd(unsigned int i) const { return feedback_shift_reg(i); }

protected:
    using base::data_reg;
    using base::output_reg;
    using base::input_reg;
    using base::feedback_shift_reg;
};

//##[ implementation ]#########################################################

}} // namespace tcpa::simulator

#endif // __TCPASIM_PROCESSOR_REGISTERS_DATA_REGISTER_FILE__
